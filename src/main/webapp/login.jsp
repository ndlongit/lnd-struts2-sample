<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<title>Login Page</title>
</head>
<body>
	<div class="center">
		<h1>Login Form</h1>
		<s:actionerror />
		<%@ include file="WEB-INF/jspf/login-form.jspf"%>
	</div>
</body>
</html>


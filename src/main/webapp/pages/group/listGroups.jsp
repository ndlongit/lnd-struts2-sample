<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<tiles:insertDefinition name="tiles.default">
	<tiles:putAttribute name="title">
		<s:property value="pageTitle" />
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
		<script>
			var ajaxZoneSelector = '#ajaxZone';

			function ready() {

				//Do somethings after page loaded
				//alert('Page loaded');
			}
		</script>

		<h2>
			<s:property value="headerText" />
		</h2>

		<div id='ajaxZone' class='ajaxZone'>
			<%@ include file="GroupsGrid.jsp"%>
		</div>

	</tiles:putAttribute>
</tiles:insertDefinition>
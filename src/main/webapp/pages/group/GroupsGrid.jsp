<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<s:set var='hasResults' value="false" />
<s:set var='disableCheckAll' value=" disabled='disabled'" />
<s:if test="results != null && results.size > 0">
	<s:set var='hasResults' value="true" />
	<s:set var='disableCheckAll' value="" />
</s:if>

<script>
	var formId = 'resultsForm';
	var tableId = 'resultsTable';
	var buttonIdList = [ "deleteMulti", 'updateMulti', 'copyMulti' ];

	function updateButtonStatus() {
		var count = countSelectedByColumnIndex(tableId, 2);
		var enable = (count > 0);

		$.each(buttonIdList, function(index, id) {
			enableLinkButton($('#' + id), enable);
		});
	}
</script>

<s:form id='resultsForm' action="">
	<s:if test="%{#hasResults}">
		<div class="leftButtonGroup">
			<a id="deleteMulti" class="disabled deleteButton" href="javascript: deleteMultiItems('resultsForm', '#ajaxZone')"><s:property
					value='getText("common.action.deleteItem", {"Groups"})' /></a>
			<!-- 			<a -->
			<!-- 				id='updateMulti' href="javascript: updateMulti('resultsForm')" -->
			<%-- 				class="disabled editButton"><s:property --%>
			<%-- 					value='getText("common.action.editItem", {"Groups"})' /></a> --%>
			<!-- 			<a -->
			<!-- 				id='copyMulti' class="disabled copyButton" -->
			<%-- 				href="javascript: copyMulti('resultsForm')"><s:property --%>
			<%-- 					value='getText("common.action.copyItem", {"Groups"})' /></a> --%>
		</div>
	</s:if>
	<div class="scroll">
		<table id='resultsTable' class="indexTable alternateRow hover border-all">
			<thead>
				<tr>
					<th class='smallColumn'><s:text name="common.index.NO" /></th>
					<th class='smallColumn'><input id='checkAll' type="checkbox"
						<s:if test="%{#hasResults}"> onclick="checkAllByColumnIndex('resultsTable', this.checked, 2); updateButtonStatus()"</s:if>
						<s:else> disabled='disabled'</s:else>></th>
					<th width="6%"><s:text name="code" /></th>
					<th width="15%"><s:text name="name" /></th>
					<th width="35%"><s:text name="description" /></th>
					<th width="14%">Accounts in Group</th>
					<th class="actionsColumn"><s:text name="common.action" /></th>
				</tr>
			</thead>
			<s:if test="hasResults">
				<s:iterator value="results" status="status">
					<tr id="row_<s:property value="id"/>">
						<td class="center">${status.index + 1}</td>
						<td class="center"><input type="checkbox" name="ids" value="<s:property value="id"/>"
							onclick="verifyCheckAllByColumnIndex('checkAll', 'resultsTable', 2); updateButtonStatus()" /></td>
						<td class="uppercase"><s:url id="view" action="%{actionView}">
								<s:param name="id" value="id" />
							</s:url> <s:a href="javascript:viewInfo('%{view}', 'viewGroup')" title='%{getText("common.action.viewItem",{"Group"})}'>
								<s:property value="code" />
							</s:a>&nbsp;</td>
						<td><s:property value="name" /> &nbsp;</td>
						<td><div class="scroll">
								<s:property value="descriptionHtml" escape="false" />
							</div></td>
						<td>
							<div class="scroll">
								<ul>
									<s:iterator value="accounts" status="status">
										<li><s:property value="loginName" /></li>
									</s:iterator>
								</ul>
							</div>
						</td>
						<td class="center"><%@ include file="/WEB-INF/jspf/action-buttons.jspf"%></td>
					</tr>
				</s:iterator>
			</s:if>
		</table>
		<s:if test="%{#hasResults != true}">
			<div class="noData">
				<s:text name="common.text.noData" />
			</div>
		</s:if>
		<div class="leftButtonGroup">
			<s:url id="create" action="%{actionPreCreate}" />
			<s:a href="%{create}" cssClass="createButton">
				<s:property value='getText("common.action.createItem", {"Group"})' />
			</s:a>
		</div>
	</div>
</s:form>
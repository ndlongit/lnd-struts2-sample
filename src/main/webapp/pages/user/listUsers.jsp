<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<tiles:insertDefinition name="tiles.default">

	<tiles:putAttribute name="title">List Users</tiles:putAttribute>

	<tiles:putAttribute name="body">
		<script>
			var ajaxZoneSelector = '#ajaxZone';
		</script>

		<h2>Users list</h2>

		<div id='ajaxZone' class='ajaxZone'>
			<%@ include file="UsersGrid.jsp"%>
		</div>

	</tiles:putAttribute>
</tiles:insertDefinition>
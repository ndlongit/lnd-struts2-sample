<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<tiles:insertDefinition name="tiles.default">

	<tiles:putAttribute name="title">
		<s:property value="pageTitle" />
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
		<script>
			function ready() {

				//Do somethings after page loaded
				//alert('Page loaded');
			}
		</script>

		<h2>
			<s:property value="headerText" />
		</h2>

		<div id="error" class="error">
			<s:actionerror />
			<s:actionmessage/>
		</div>

		<s:form action="%{action}" validate="true">
			<s:textfield id="loginName" key="loginName" name="loginName" maxlength="20" required="true" />
			<s:password id="password" key="password" name="password" maxlength="15" />
			<s:password id="password2" key="password2" name="password2" maxlength="15" />
			<s:textfield id="firstName" key="firstName" name="firstName" required="true" />
			<s:textfield id="lastName" key="lastName" name="lastName" required="true" />
			<s:textfield id="email" key="email" name="email" required="true" />
			<s:checkboxlist id="groups" list="groupModels" name="groupValues" listKey="id" listValue="name" key="groups" />
			<s:checkboxlist id="roles" list="roleModels" name="roleValues" listKey="id" listValue="name" key="roles" />

			<s:if test="%{action==actionCreate}">
				<s:submit name="create" value='%{getText("common.action.createItem", {"User"})}' class="center" />
			</s:if>
			<s:elseif test="%{action==actionCopy}">
				<s:submit name="copy" value='%{getText("common.action.copyItem", {"User"})}' class="center" />
			</s:elseif>
			<s:elseif test="%{action==actionEdit}">
				<s:submit name="edit" key="common.action.edit" class="center" />
			</s:elseif>

			<!-- Hidden fields - Begin -->
			<s:hidden id="actionName" name="action" />
			<s:hidden id="entityId" name="id" />
			<!-- Hidden fields - End -->
		</s:form>
	</tiles:putAttribute>

</tiles:insertDefinition>

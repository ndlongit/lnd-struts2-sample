<%@ taglib prefix="s" uri="/struts-tags"%>

<s:set var='hasResults' value="false" />
<s:set var='disableCheckAll' value=" disabled='disabled'" />
<s:if test="results != null && results.size > 0">
	<s:set var='hasResults' value="true" />
	<s:set var='disableCheckAll' value="" />
</s:if>

<script>
	var formId = 'resultsForm';
	var tableId = 'resultsTable';
	var buttonIdList = [ "deleteMulti", 'updateMulti', 'copyMulti' ];

	function updateButtonStatus() {
		var count = countSelectedByColumnIndex(tableId, 2);
		var enable = (count > 0);

		$.each(buttonIdList, function(index, id) {
			enableLinkButton($('#' + id), enable);
		});
	}
</script>

<s:form id='resultsForm' action="">
	<!-- 	<div class="center"> -->
	<%-- 			<span class="pagination"></span> --%>
	<!-- 	</div> -->
	<s:if test="%{#hasResults}">
		<div class="leftButtonGroup">
			<a id="deleteMulti" class="disabled deleteButton" href="javascript: deleteMultiItems('resultsForm', '#ajaxZone')"><s:property
					value='getText("common.action.deleteItem", {"Users"})' /></a><a id='updateMulti' href="javascript: updateMulti('resultsForm')" class="disabled editButton"><s:property
					value='getText("common.action.editItem", {"Users"})' /></a><a id='copyMulti' class="disabled copyButton" href="javascript: copyMulti('resultsForm')"><s:property
					value='getText("common.action.copyItem", {"Users"})' /></a>
		</div>
		<div class="clearBoth"></div>
	</s:if>

	<div class="scroll">
		<table id='resultsTable' fixedRows="1" fixedColumns="3" class="indexTable sortTable alternateRow hover border-all">
			<thead>
				<tr>
					<th class='sorter-false smallColumn'><s:text name="common.index.NO" /></th>
					<th class='sorter-false smallColumn'><input id='checkAll' type="checkbox"
						<s:if test="%{#hasResults}"> onclick="checkAllByColumnIndex('resultsTable', this.checked, 2); updateButtonStatus()"</s:if>
						<s:else> disabled='disabled'</s:else>></th>
					<th width="10%"><s:text name="loginName" /></th>
					<th>Full Name</th>
					<th width="14%"><s:text name="email" /></th>
					<th width="14%"><s:text name="groups" /></th>
					<th width="14%"><s:text name="roles" /></th>
					<th class="sorter-false actionsColumn"><s:text name="common.action" /></th>
				</tr>
			</thead>
			<tbody>
				<s:if test="hasResults">
					<s:iterator value="results" status="status">
						<tr id="row_<s:property value="id"/>">
							<td class="center">${status.index + 1}</td>
							<td class="center"><input type="checkbox" name="ids" value="<s:property value="id"/>"
								onclick="verifyCheckAllByColumnIndex('checkAll', 'resultsTable', 2); updateButtonStatus()" /></td>
							<td><s:url id="view" action="%{actionView}">
									<s:param name="id" value="id" />
								</s:url> <s:a href="javascript: viewInfo('%{view}', 'viewUser')" title='%{getText("common.action.viewItem",{"User"})}'>
									<s:property value="loginName" />
								</s:a>&nbsp;								
								</td>
							<td class="capitalize"><s:property value="fullName" />&nbsp;</td>
							<td><s:if test="email != null && email != ''">
									<a href="mailto:<s:property value="email"/>?subject=Email%20Subject" title="Click to send an email"><s:property value="email" /></a>
								</s:if>&nbsp;</td>
							<td>
								<div class="scroll">
									<ul>
										<s:iterator value="groups" status="status">
											<li><s:property value="name" /></li>
										</s:iterator>
									</ul>
								</div>
							</td>
							<td>
								<div class="scroll">
									<ul>
										<s:iterator value="roles" status="status">
											<li><s:property value="name" /></li>
										</s:iterator>
									</ul>
								</div>
							</td>
							<td class="center"><%@ include file="/WEB-INF/jspf/action-buttons.jspf"%></td>
						</tr>
					</s:iterator>
				</s:if>
			</tbody>
		</table>
		<s:if test="%{#hasResults != true}">
			<s:if test="%{submitted == true}">
				<div class="noData">
					<s:text name="common.text.noData" />
				</div>
			</s:if>
		</s:if>

		<s:url id="create" action="%{actionPreCreate}" />
		<div class="leftButtonGroup">
			<s:a href="%{create}" cssClass="createButton">
				<s:property value='getText("common.action.createItem", {"User"})' />
			</s:a>
		</div>
	</div>
</s:form>

<script type="text/javascript">
	//$('#' + tableId).tablesorter(); // Conflict with fixTable() feature
	fixTable($('#' + tableId)); //Call it after Ajax complete instead
	updateButtonStatus();

	$(".pagination").pagination({
		items : 10,
		displayedPages : 3
	});
</script>

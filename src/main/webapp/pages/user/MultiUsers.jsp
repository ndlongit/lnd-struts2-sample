<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<s:set var='disableCheckAll' value=" disabled='disabled'" />
<s:if test="results != null && results.size > 0">
	<s:set var='hasResults' value="true" />
	<s:set var='disableCheckAll' value="" />
</s:if>

<s:form id='resultsForm' theme="simple" action="%{actionMulti}">
	<div class="scroll">
		<table id='resultsTable' class="border-all">
			<thead>
				<tr>
					<th class='smallColumn'><s:text name="common.index.NO" /></th>
					<th width="15%"><s:text name="loginName" /></th>
					<th width="15%"><s:text name="firstName" /></th>
					<th width="15%"><s:text name="lastName" /></th>
					<th width="20%"><s:text name="email" /></th>
				</tr>
			</thead>
			<s:if test="hasResults">
				<tbody>
					<s:iterator value="results" status="status">
						<tr id="row_<s:property value="id"/>">
							<td class="center">${status.index + 1}<s:hidden name="results[%{#status.index}].id" /></td>
							<td><s:textfield name="results[%{#status.index}].loginName" maxlength="20" /></td>
							<td><s:textfield name="results[%{#status.index}].firstName" /></td>
							<td><s:textfield name="results[%{#status.index}].lastName" /></td>
							<td><s:textfield name="results[%{#status.index}].email" /></td>
						</tr>
					</s:iterator>
				</tbody>
			</s:if>
		</table>
		<s:if test="%{#hasResults != true}">
			<div class="noData">
				<s:text name="common.text.noData" />
			</div>
		</s:if>

		<s:url id="create" action="%{actionCreate}" />
		<div class="leftButtonGroup">
			<s:a href="%{create}" cssClass="createButton">
				<s:property value='getText("common.action.createItem", {"User"})' />
			</s:a>
		</div>

		<div class="rightButtonGroup">
			<s:if test="hasResults">
				<s:submit value="Submit" cssClass="submitButton" />
			</s:if>
		</div>
	</div>
</s:form>

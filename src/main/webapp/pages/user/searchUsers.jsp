<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<tiles:insertDefinition name="tiles.default">

	<tiles:putAttribute name="title">
		<s:property value="pageTitle" />
	</tiles:putAttribute>

	<tiles:putAttribute name="body">

		<script>
			var ajaxZoneSelector = '#ajaxZone';

			function searchUser() {
				submitFormAjax('searchUserForm', ajaxZoneSelector, testCallback);
			}

			function updateMultiUsers() {
				var actionMethod = "<s:property value='%{actionPreEditMulti}' />";
				submitForm('resultsForm', actionMethod); //common.js
			}

			function testCallback(jqXHR, textStatus) {
				//alert(jqXHR);
			}
		</script>
		<h2>
			<s:property value="headerText" />
		</h2>
		<table>
			<tr>
				<td valign="bottom" width="350px"><s:form method="post" action="search" id="searchUserForm">
						<s:textfield id="loginName" key="loginName" name="user.loginName" maxlength="20" />
						<s:textfield id="firstName" key="firstName" name="user.firstName" />
						<s:textfield id="lastName" key="lastName" name="user.lastName" />
						<s:textfield id="email" key="email" name="user.email" />
						<s:textfield id="groupCode" key="groupCode" name="group.code" />
						<s:textfield id="groupName" key="groupName" name="group.name" />
						<s:checkbox name="submitted" value="true" cssClass="hidden" />
					</s:form></td>
				<td align="left" valign="bottom"><input type="button" style="margin-bottom: 17px" class="submitButton" onclick="searchUser()" value="Search Users"></td>
			</tr>
		</table>

		<div id='ajaxZone' class='ajaxZone'>
			<%@ include file="UsersGrid.jsp"%>
		</div>
	</tiles:putAttribute>

</tiles:insertDefinition>
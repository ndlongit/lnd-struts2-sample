<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<s:set var='hasResults' value="false" />
<s:set var='disableCheckAll' value=" disabled='disabled'" />
<s:if test="employees != null && employees.size > 0">
	<s:set var='hasResults' value="true" />
	<s:set var='disableCheckAll' value="" />
</s:if>

<script>
	var formId = 'resultsForm';
	var tableId = 'resultsTable';
	var buttonIdList = [ "deleteMulti", 'updateMulti', 'copyMulti' ];

	function updateButtonStatus() {
		var count = countSelectedByColumnIndex(tableId, 2);
		var enable = (count > 0);

		$.each(buttonIdList, function(index, id) {
			enableLinkButton($('#' + id), enable);
		});
	}
</script>

<s:form id='resultsForm' action="">
	<s:if test="%{#hasResults}">
		<div class="leftButtonGroup">
			<a id="deleteMulti" class="disabled deleteButton"
				href="javascript: deleteMultiItems('resultsForm', '#ajaxZone')"><s:property
					value='getText("common.action.deleteItem", {"Employees"})' /></a><a
				id='updateMulti' href="javascript: updateMulti('resultsForm')"
				class="disabled editButton"><s:property
					value='getText("common.action.editItem", {"Employees"})' /></a><a
				id='copyMulti' class="disabled copyButton"
				href="javascript: copyMulti('resultsForm')"><s:property
					value='getText("common.action.copyItem", {"Employees"})' /></a>
		</div>
	</s:if>
	<div class="scroll">
		<table id='resultsTable'
			class="indexTable alternateRow hover border-all">
			<thead>
				<tr>
					<th class='smallColumn'><s:text name="common.index.NO" /></th>
					<th class='smallColumn'><input id='checkAll' type="checkbox"
						<s:if test="%{#hasResults}"> onclick="checkAllByColumnIndex('resultsTable', this.checked, 2); updateButtonStatus()"</s:if>
						<s:else> disabled='disabled'</s:else>></th>
					<th width="14%">Employee Code</th>
					<th width="20%">Full Name</th>
					<th width="15%"><s:text name="email" /></th>
					<th width="10%"><s:text name="department" /></th>
					<th class="actionsColumn"><s:text name="common.action" /></th>
				</tr>
			</thead>
			<s:if test="%{#hasResults}">
				<s:iterator value="employees" status="status">
					<tr id="row_<s:property value="id"/>">
						<td class="center">${status.index + 1}</td>
						<td class="center"><input type="checkbox" name="ids"
							value="<s:property value="id"/>"
							onclick="verifyCheckAllByColumnIndex('checkAll', 'resultsTable', 2); updateButtonStatus()" /></td>
						<td class="uppercase"><s:url id="view" action="%{actionView}">
								<s:param name="id" value="id" />
							</s:url> <s:a href="javascript:viewInfo('%{view}', 'viewEmployee')"
								title='%{getText("common.action.viewItem",{"Employee"})}'>
								<s:property value="code" />
							</s:a>&nbsp;</td>
						<td class="capitalize"><s:property value="fullName" />&nbsp;</td>
						<td><s:if test="email != null && email != ''">
								<a
									href="mailto:<s:property value="email"/>?subject=Email%20Subject"
									title="Click to send an email"><s:property value="email" /></a>
							</s:if>&nbsp;</td>
						<td><s:url id="viewDepartment" action="%{actionView}"
								namespace="%{@lnd.sample.web.action.department.DepartmentAction@NAME_SPACE}">
								<s:param name="id" value="department.id" />
							</s:url> <s:a
								href="javascript:viewInfo('%{viewDepartment}', 'viewDepartment')"
								title='%{getText("common.action.viewItem",{"Department"})}'>
								<s:property value="department.name" />
							</s:a>&nbsp;</td>
						<td class="center"><%@ include
								file="/WEB-INF/jspf/action-buttons.jspf"%></td>
					</tr>
				</s:iterator>
			</s:if>
		</table>
		<s:if test="%{#hasResults != true}">
			<div class="noData">
				<s:text name="common.text.noData" />
			</div>
		</s:if>
		<div class="leftButtonGroup">
			<s:url id="create" action="%{actionPreCreate}" />
			<s:a href="%{create}" cssClass="createButton">
				<s:property
					value='getText("common.action.createItem", {"Employee"})' />
			</s:a>
		</div>
	</div>
</s:form>

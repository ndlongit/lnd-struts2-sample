<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<tiles:insertDefinition name="tiles.default">

	<tiles:putAttribute name="title">List Employees</tiles:putAttribute>

	<tiles:putAttribute name="body">
		<script>
			var ajaxZoneSelector = '#ajaxZone';

			function ready() {

				//Do somethings after page loaded
				//alert('Page loaded');
			}
		</script>

		<h2>Employee list</h2>

		<div id='ajaxZone' class='ajaxZone'>
			<%@ include file="EmployeesGrid.jsp"%>
		</div>
	</tiles:putAttribute>

</tiles:insertDefinition>
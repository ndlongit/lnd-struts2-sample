<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<tiles:insertDefinition name="tiles.default">

	<tiles:putAttribute name="title">List Roles</tiles:putAttribute>

	<tiles:putAttribute name="body">
		<script>
			var ajaxZoneSelector = '#ajaxZone';

			function view(url) {
				openPopup(url, 800, 600, 'viewRole');
			}

			function deleteMultiple() {
				$('form').first().submit();
			}
		</script>

		<div id="ajaxZone" class='ajaxZone'><%@ include
				file="RolesGrid.jsp"%></div>
	</tiles:putAttribute>

</tiles:insertDefinition>
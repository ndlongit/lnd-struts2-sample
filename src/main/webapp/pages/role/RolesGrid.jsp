<%@ taglib prefix="s" uri="/struts-tags"%>

<s:set var='hasResults' value="false" />
<s:set var='disableCheckAll' value=" disabled='disabled'" />
<s:if test="roles != null && roles.size > 0">
	<s:set var='hasResults' value="true" />
	<s:set var='disableCheckAll' value="" />
</s:if>

<script>
	var formId = 'resultsForm';
	var tableId = 'resultsTable';
	var buttonIdList = [ "deleteMulti" ];

	function updateButtonStatus() {
		var count = countSelectedByColumnIndex(tableId, 2);
		var enable = (count > 0);

		$.each(buttonIdList, function(index, id) {
			enableLinkButton($('#' + id), enable);
		});
	}
</script>

<s:form id='resultsForm' action="">
	<h2>Role list</h2>
	<s:if test="%{#hasResults}">
		<div class="leftButtonGroup">
			<a id="deleteMulti"
				href="javascript: deleteMultiItems('resultsForm', '#ajaxZone')"
				class="disabled deleteButton"><s:property
					value='getText("common.action.deleteItem", {"Roles"})' /></a>
		</div>
	</s:if>
	<div class="scroll">
		<table id='resultsTable'
			class="indexTable alternateRow hover border-all">
			<thead>
				<tr>
					<th class='smallColumn'><s:text name="common.index.NO" /></th>
					<th class='smallColumn'><input id='checkAll' type="checkbox"
						<s:if test="%{#hasResults}"> onclick="checkAllByColumnIndex('resultsTable', this.checked, 2); updateButtonStatus()"</s:if>
						<s:else> disabled='disabled'</s:else>></th>
					<th width="15%"><s:text name="code" /></th>
					<th width="15%"><s:text name="name" /></th>
					<th width="30%"><s:text name="description" /></th>
					<th class="actionsColumn"><s:text name="common.action" /></th>
				</tr>
			</thead>
			<s:if test="hasResults">
				<s:iterator value="roles" status="status">
					<tr id="row_<s:property value="id"/>">
						<td class="center">${status.index + 1}</td>
						<td class="center"><input type="checkbox" name="ids"
							value="<s:property value="id"/>"
							onclick="verifyCheckAllByColumnIndex('checkAll', 'resultsTable', 2); updateButtonStatus()" /></td>
						<td class="uppercase"><s:url id="view" action="%{actionView}">
								<s:param name="id" value="id" />
							</s:url> <s:a href="javascript:view('%{view}')"
								title='%{getText("common.action.viewItem",{"Role"})}'>
								<s:property value="code" />
							</s:a>&nbsp;</td>
						<td><s:property value="name" />&nbsp;</td>
						<td>
							<div class="scroll">
								<s:property value="descriptionHtml" escape="false" />
							</div>
						</td>
						<td class="center"><%@ include
								file="/WEB-INF/jspf/action-buttons.jspf"%></td>
					</tr>
				</s:iterator>
			</s:if>
		</table>
		<s:if test="%{#hasResults != true}">
			<div class="noData">
				<s:text name="common.text.noData" />
			</div>
		</s:if>

		<div class="leftButtonGroup">
			<s:url id="create" action="%{actionPreCreate}" />
			<s:a href="%{create}" cssClass="createButton">
				<s:property value='getText("common.action.createItem", {"Role"})' />
			</s:a>
		</div>
	</div>
</s:form>
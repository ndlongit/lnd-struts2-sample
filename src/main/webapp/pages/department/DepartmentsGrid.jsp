<%@ taglib prefix="s" uri="/struts-tags"%>

<s:set var='hasResults' value="false" />
<s:set var='disableCheckAll' value=" disabled='disabled'" />
<s:if test="departments != null && departments.size > 0">
	<s:set var='hasResults' value="true" />
	<s:set var='disableCheckAll' value="" />
</s:if>

<s:form
	action="%{@lnd.sample.web.action.base.AbstractAction@ACTION_DELETE_MULTIPLE}">
	<h2>Department list</h2>
	<s:if test="%{#hasResults}">
		<span> <a href="javascript: deleteMultiple()"
			class="deleteButton"><s:property
					value='getText("common.action.deleteItem", {"Departments"})' /></a>
		</span>
	</s:if>
	<div class="scroll">
		<table id='resultsTable'
			class="indexTable alternateRow hover border-all">
			<thead>
				<tr>
					<th class='smallColumn'><s:text name="common.index.NO" /></th>
					<th class='smallColumn'><input id='checkAll' type="checkbox"
						<s:if test="%{#hasResults}"> onclick="checkAllByColumnIndex('resultsTable', this.checked, 2)"</s:if>
						<s:else> disabled='disabled'</s:else>></th>
					<th width="15%"><s:text name="code" /></th>
					<th width="15%"><s:text name="name" /></th>
					<th width="30%"><s:text name="description" /></th>
					<th class="actionsColumn"><s:text name="common.action" /></th>
				</tr>
			</thead>
			<s:if test="hasResults">
				<s:iterator value="departments" status="status">
					<tr id="row_<s:property value="id"/>">
						<td class="center">${status.index + 1}</td>
						<td class="center"><input type="checkbox" name="ids"
							value="<s:property value="id"/>"
							onclick="verifyCheckAllByColumnIndex('checkAll', 'resultsTable', 2)" /></td>
						<td class="uppercase"><s:url id="view" action="%{actionView}">
								<s:param name="id" value="id" />
							</s:url> <s:a href="javascript:view('%{view}')"
								title='%{getText("common.action.viewItem",{"Department"})}'>
								<s:property value="code" />
							</s:a>&nbsp;</td>
						<td><s:property value="name" />&nbsp;</td>
						<td><s:property value="descriptionHtml" escape="false" />&nbsp;</td>
						<td class="center"><%@ include
								file="/WEB-INF/jspf/action-buttons.jspf"%></td>
					</tr>
				</s:iterator>
			</s:if>
		</table>
		<s:if test="%{#hasResults != true}">
			<div class="noData">
				<s:text name="common.text.noData" />
			</div>
		</s:if>

		<s:url id="create" action="%{actionPreCreate}" />
		<s:a href="%{create}" cssClass="createButton">
			<s:property
				value='getText("common.action.createItem", {"Department"})' />
		</s:a>
	</div>
</s:form>
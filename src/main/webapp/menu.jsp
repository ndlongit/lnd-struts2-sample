<%@ taglib prefix="s" uri="/struts-tags"%>

<h2>
	<s:text name="common.text.mainMenu" />
</h2>
<ul>
	<li><a href="<s:url value="/home" />"><s:text name="common.menu.home" /></a></li>
	
	<!-- Group -->
	<li><a href="<s:url value="/group/preCreate" />">Create Group</a></li>
	<li><a href="<s:url value="/group/list" />">List Groups</a></li>

	<!-- Role -->
	<li><a href="<s:url value="/role/preCreate" />">Create Role</a></li>
	<li><a href="<s:url value="/role/list" />">List Roles</a></li>

	<!-- User -->
	<li><a href="<s:url value="/user/preCreate" />"><s:property value='getText("common.action.createItem", {"User"})' /></a></li>
	<li><a href="<s:url value="/user/list" />"><s:text name="menu.list.user" /></a></li>
	<li><a href="<s:url value="/user/search" />">Search Users</a></li>

	<!-- Employee -->
	<%-- 	<li><a href="<s:url value="/employee/preCreate" />"><s:property value='getText("common.action.createItem", {"Employee"})' /></a></li> --%>
	<%-- 	<li><a href="<s:url value="/employee/list" />">List Employees</a></li> --%>

	<!-- Department -->
	<%-- 	<li><a href="<s:url value="/department/preCreate" />">Create Department</a></li> --%>
	<%-- 	<li><a href="<s:url value="/department/list" />">List Departments</a></li> --%>
</ul>

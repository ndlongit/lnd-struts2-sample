function callAjax(fullURL) {
	$.ajax({
		type : "POST",
		url : fullURL,
		success : handleSuccess,
		error : handleError,
		complete : handleComplete,
		statusCode : {
			404 : function() {
				alert("Requested page not found");
			},
			500 : function() {
				alert("Internal Server Error");
			}
		}
	});
}

/**
 * @param :
 *            textStatus = success | notmodified | error | timeout | abort |
 *            parsererror
 */
function handleComplete(jqXHR, textStatus) {
	log('handleComplete: ' + textStatus);
}

function handleSuccess(data, textStatus, jqXHR) {
	log('handleSuccess: ' + textStatus);
}

function handleError(jqXHR, textStatus, errorThrown) {
	log('handleError: ' + textStatus);
}
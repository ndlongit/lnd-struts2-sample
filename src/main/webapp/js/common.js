//var common = new Object();
var disabledClass = 'disabled';
var disabledAction = 'return false;';

function submitForm(formId, actionMethod) {
	var mainForm = $('form#' + formId);
	if (actionMethod) {
		var baseAction = mainForm.prop('action');
		mainForm.prop('action', baseAction + actionMethod);
	}

	mainForm.submit();
}

/** Remove heading and trailing spaces of a string */
function trim(value) {
	if (value == null) {
		return null;
	}

	// Remove heading spaces
	value = value.replace(/^\s+/, '');

	// Remove trailing spaces
	value = value.replace(/\s+$/, '');
	return value;
}

/** Remove heading and trailing spaces of value of a HTML control */
function trimValue(control) {
	if (!control) {
		return;
	}

	var value = trim($(control).val());
	$(control).val(value);
}

function openPopup(url, width, height, windowName, options) {
	if (!width) {
		width = 800;
	}

	if (!height) {
		height = 600;
	}

	if (!windowName) {
		windowName = "_blank";
	}

	if (!options) {
		options = "fullscreen=yes, location=yes, menubar=yes, resizable=yes, scrollbars=yes, status=yes, titlebar=yes, toolbar=yes, top=o, left=0";
	}

	var winSize = ", width=" + width + ", height=" + height;
	var specs = options + winSize;

	var win = window.open(url, windowName, specs);
	if (win) {
		win.focus();
		win.moveTo(0, 0);
	}
}

function log(message) {
	if (console && console.log) {
		console.log(message);
	}
}

/*
 * Check/Uncheck all check-boxes of a column in a table
 * 
 * Parameters : checkedStatus: selected status of the main check-box, default is
 * false tableId : ID of the table columnIndex : Index of the column in the
 * table, starting from 1, default = 1.
 */
var noneDisable = ":not(:disabled)";
function checkAllByColumnIndex(tableId, checkedStatus, columnIndex) {
	if (!checkedStatus) {
		checkedStatus = false;
	}

	if (!columnIndex || columnIndex < 1) {
		columnIndex = 1;
	}

	var tableSelector = "#" + tableId + " tr td:nth-child(" + columnIndex + ")";
	var checkboxSelector = "input[type=checkbox]";

	// process all, except disabled ones
	checkboxSelector += noneDisable;

	$(tableSelector + " " + checkboxSelector).prop("checked", checkedStatus);
}

function countSelectedByColumnIndex(tableId, columnIndex) {
	var tableSelector = "#" + tableId + " tr td:nth-child(" + columnIndex + ")";
	var checkboxSelector = "input:checked" + noneDisable;
	return ($(tableSelector + " " + checkboxSelector).size());
}

/**
 * Verify to set check/un-check status for check-all checkbox with id
 * checkAllCheckboxId
 * 
 * @param checkAllCheckboxId:
 *            ID of check-all checkbox
 * 
 * @param tableId:
 *            ID of the table columnIndex: Column index of the table, starting
 *            from 1, default = 1
 */
function verifyCheckAllByColumnIndex(checkAllCheckboxId, tableId, columnIndex) {
	if (!columnIndex) {
		columnIndex = 1;
	}

	var tableSelector = "#" + tableId + " tr td:nth-child(" + columnIndex + ")";
	var checkboxSelector = "input[type=checkbox]:not(:disabled)";

	var checked = true;
	$(tableSelector + " " + checkboxSelector).each(function(index) {
		checked = $(this).prop("checked");
		if (!checked || checked == false) {
			return false;
		}
	});

	$("#" + checkAllCheckboxId + ':not(:disabled)').prop("checked", checked);
}

/**
 * Check/Uncheck all check-boxes in a container
 * 
 * @param parentId :
 *            ID of the container
 * @param checkedStatus :
 *            status is checked or unchecked, default is false
 */
function checkAllByParent(parentId, checkedStatus) {
	if (!checkedStatus) {
		checkedStatus = false;
	}

	var parentSelector = "#" + parentId;
	var checkboxSelector = "input[type=checkbox]";

	// process all, except disabled ones
	checkboxSelector += noneDisable;

	$(parentSelector + " " + checkboxSelector).prop("checked", checkedStatus);
}

/**
 * @param callbackFunction:
 *            Get passed 4 parameters. String textStatus (success | notmodified |
 *            error | timeout | abort | parsererror), PlainObject data, jqXHR
 *            jqXHR and String errorThrown (HTTP error message)
 * @param fullURL
 *            the URL with query string
 */

var loadingClass = 'loading';
function sendAjaxRequest(fullURL, ajaxZoneSelector, callbackFunction) {

	var errorMessage = ''; // HTTP error message
	if (ajaxZoneSelector && $(ajaxZoneSelector)) {
		$(ajaxZoneSelector).html("<div class='" + loadingClass + "' />");
	}

	$.ajax({
		type : "POST",
		url : fullURL,
		error : function(jqXHR, textStatus, errorThrown) {
			errorMessage = errorThrown;
		},
		complete : function(jqXHR, textStatus) {
			handleAjaxComplete(jqXHR, textStatus, ajaxZoneSelector);

			if (typeof callbackFunction == 'function') {
				callbackFunction(jqXHR, textStatus, errorMessage);
			}
		}
	});
}

function submitFormAjax(formId, ajaxZoneSelector, callbackFunction) {
	var fullUrl = buildFullUrl(formId);
	sendAjaxRequest(fullUrl, ajaxZoneSelector, callbackFunction);
}

function buildFullUrl(formId, actionMethod) {
	var action = $('form#' + formId).attr('action');
	if (actionMethod) {
		action += '/' + actionMethod;
	}

	var params = $('form#' + formId).serialize();
	var fullUrl = action + '?' + params;

	return fullUrl;
}

function handleAjaxComplete(jqXHR, textStatus, ajaxZoneSelector) {
	if ((textStatus == 'success')) {
		handleAjaxSuccess(jqXHR, ajaxZoneSelector);
	} else {
		handleAjaxError(jqXHR);
	}
}

/*
 * , statusCode : { 404 : function() { log("Requested page not found"); }, 500 :
 * function() { log("Internal Server Error"); } }
 */

function handleAjaxSuccess(jqXHR, ajaxZoneSelector) {
	if (ajaxZoneSelector) {
		$(ajaxZoneSelector).html(jqXHR.responseText);
		//$(ajaxZoneSelector).slideUp(200).fadeIn(2000);
		/*
		 * var cssClass = 'ajaxZone-update';
		 * $(ajaxZoneSelector).addClass(cssClass);
		 * $(ajaxZoneSelector).removeClass(cssClass);
		 */
	}
}

function handleAjaxError(jqXHR) {
	var headerName = 'errorMessage';
	var errorMessage = jqXHR.getResponseHeader(headerName);
	if (!errorMessage || errorMessage == '') {
		errorMessage = jqXHR.statusText;
	}

	alert(errorMessage);
}

/**
 * Convert New Line characters to <br>
 * tags
 */
function nl2br(text) {
	if (!text) {
		return text;
	}

	return text.replace(/(\r\n|\n\r|\r|\n)/g, "<br>");
}

function nl2br(text) {
	if (!text) {
		return text;
	}

	return text.replace(/<br>/g, "\n");
}

function enableControl(id, enable) {
	var control = $('#' + id);
	
	if (enable == true) {
		control.prop("disabled", null);
		control.removeClass(disabledClass);
	} else {
		control.prop("disabled", true);
		control.addClass(disabledClass);
	}
}

function enableLinkButton(button, enable) {
	if (!button) {
		return;
	}

	if (enable == true) {
		button.removeClass(disabledClass);
		button.attr('onclick', null);
	} else {
		button.addClass(disabledClass);
		button.attr('onclick', disabledAction);
	}
}
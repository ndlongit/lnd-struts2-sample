/** Define/Customize some JS functions - Begin */

/** indexOf() */
if (!Array.indexOf) {
	Array.prototype.indexOf = function(obj, start) {
		for (var i = (start || 0); i < this.length; i++) {
			if (this[i] == obj) {
				return i;
			}
		}
		return -1;
	};
}
/** Define/Customize some JS functions - End */

<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<html>
<head>
<title><tiles:getAsString name="title" /></title>
<s:head />

<!-- JSP page contains dynamic CSS-->
<jsp:include page="/WEB-INF/jspf/dynamic-css.jsp"></jsp:include>
<tiles:insertAttribute name="before-css" />
<link href="<s:url value="/css/style.css"/>" rel="stylesheet"
	type="text/css" />
<tiles:insertAttribute name="after-css" />
<tiles:insertAttribute name="dynamic-css" />

<!-- JSP page contains dynamic JavaScript-->
<jsp:include page="/WEB-INF/jspf/dynamic-js.jsp"></jsp:include>

<tiles:insertAttribute name="before-js" />
<script type="text/javascript" src="<s:url value='/js/global.js'/>"></script>
<script type="text/javascript" src="<s:url value='/js/common.js'/>"></script>
<script type="text/javascript"
	src="<s:url value='/js/jquery/jquery-2.1.0.js'/>"></script>

<!-- Plugins - Begin -->
<script type="text/javascript"
	src="<s:url value='/js/jquery/jquery.blockUI.js'/>"></script>
<script type="text/javascript"
	src="<s:url value='/js/jquery/jquery.tablefix.js'/>"></script>
<script type="text/javascript"
	src="<s:url value='/js/plugins/pagination/jquery.simplePagination.js'/>"></script>
<link type="text/css" rel="stylesheet"
	href="<s:url value='/js/plugins/pagination/simplePagination-all.css'/>" />

<!-- Tablesorter - Begin -->
<script type="text/javascript"
	src="<s:url value='/js/plugins/tablesorter/jquery.tablesorter.js'/>"></script>
<link type="text/css" rel="stylesheet"
	href="<s:url value='/js/plugins/tablesorter/tablesorter-all.css'/>">
<!-- Tablesorter - End -->

<!-- Validation - Begin -->
<script type="text/javascript"
	src="<s:url value='/js/plugins/validation/jquery.validate.js'/>"></script>
<!-- Validation - End -->
<!-- Plugins - End -->

<tiles:insertAttribute name="after-js" />
<tiles:insertAttribute name="dynamic-js" />

<script>
	function onload() {
		window.focus();

		if (typeof ready == 'function') {
			ready();
		}

		$(".disabled").focus(function() {
			$(this).blur();
		});

		//Call it before calling setFixedTables();
		//Currently, it is conflict with setFixedTables()
		setSortTables();

		setFixedTables();
		disableLinkButtons();
		formatIndexNumber(2);

	}

	function setFixedTables() {
		$('.fixedTable').each(function() {
			fixTable($(this));
		});
	}

	function fixTable(table) {
		if (!table) {
			return;
		}

		var fixedWidth = table.attr('fixedWidth');
		if (!fixedWidth || fixedWidth <= 0) {
			fixedWidth = table.width();
		}
		var fixedHeight = table.attr('fixedHeight');
		if (!fixedHeight) {
			fixedHeight = 350;
		}

		var fixedRows = table.attr('fixedRows');
		//var fixedColumns = table.attr('fixedColumns');

		$(table).tablefix({
			width : fixedWidth * 1,
			height : fixedHeight * 1,
			fixRows : fixedRows * 1
		});
	}

	function setSortTables() {
		$('.sortTable').each(function() {
			sortTable($(this).prop('id'));
		});
	}

	function sortTable(tableId, options) {
		if (options) {
			$("#" + tableId).tablesorter(options);
		} else {
			$("#" + tableId).tablesorter();
		}
	}

	function disableLinkButtons() {
		var cssClass;
		if (typeof disabledClass != 'undefined') {
			cssClass = disabledClass;
		} else {
			cssClass = 'disabled';
		}

		$('.' + cssClass).each(function() {
			enableLinkButton($(this), false);
		});
	}

	function formatIndexNumber(minLength) {
		if (!minLength) {
			minLength = 2;
		}

		$('table.indexTable tbody tr').each(function(i) {
			var cell = $(this).find('td:nth-child(1)');

			if (!cell) {
				return false;
			}

			var text = cell.text();
			var textLength = text.length;
			for (var i = textLength; i < minLength; i++) {
				text = '0' + text;
			}

			cell.text(text);
		});
	}

	function viewInfo(url, name) {
		openPopup(url, 800, 600, name);
	}

	function updateMulti(formId) {
		var actionMethod = "<s:property value='%{actionPreEditMulti}' />";
		submitForm(formId, actionMethod);
	}

	function copyMulti(formId) {
		var actionMethod = "<s:property value='%{actionPreCopyMulti}' />";
		submitForm(formId, actionMethod);
	}

	function deleteMultiItems(formId, zoneSelector) {
		var actionMethod = "<s:property value='%{actionDeleteMulti}' />";
		var fullURL = buildFullUrl(formId, actionMethod);

		var content = 'Are you sure you want to delete selected item(s)?';
		var okCallback = 'sendAjaxRequest("' + fullURL + '", "' + zoneSelector
				+ '")';

		showConfirm(content, okCallback); //from common-dialogs.jspf
	}
</script>
</head>

<body onload="onload();">
	<table>
		<tr>
			<td colspan="2"><tiles:insertAttribute name="header" /></td>
		</tr>
		<tr align="left" valign="top">
			<td nowrap width="16%"><tiles:insertAttribute name="menu" /></td>
			<td><tiles:insertAttribute name="body" /></td>
		</tr>
		<tr>
			<td colspan="2"><tiles:insertAttribute name="footer" /></td>
		</tr>
	</table>
	<!-- Dialog Boxes -->
	<jsp:include page="/WEB-INF/jspf/common-dialogs.jspf"></jsp:include>
</body>
</html>
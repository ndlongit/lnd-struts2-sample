package lnd.sample.service;

import java.util.List;

import org.springframework.stereotype.Service;

import lnd.sample.dao.RoleDao;
import lnd.sample.model.Role;
import lnd.sample.service.base.AbstractService;

@Service
public class RoleServiceImpl extends AbstractService<Role, Long, RoleDao> implements RoleService {

    @Override
    public List<Role> getByNames(List<String> roleNames) {
        return dao.getByNames(roleNames);
    }
}

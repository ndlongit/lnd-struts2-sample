package lnd.sample.service;

import lnd.sample.model.Employee;
import lnd.sample.service.base.BasicService;

public interface EmployeeService extends BasicService<Employee, Long> {
}

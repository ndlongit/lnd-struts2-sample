package lnd.sample.service;

import lnd.sample.model.Group;
import lnd.sample.service.base.BasicService;

public interface GroupService extends BasicService<Group, Long> {
}

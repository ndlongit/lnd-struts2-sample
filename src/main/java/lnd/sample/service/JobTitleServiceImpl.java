package lnd.sample.service;

import lnd.sample.dao.JobTitleDao;
import lnd.sample.model.JobTitle;
import lnd.sample.service.base.AbstractService;

import org.springframework.stereotype.Service;

@Service
public class JobTitleServiceImpl extends AbstractService<JobTitle, Long, JobTitleDao> implements JobTitleService {
}

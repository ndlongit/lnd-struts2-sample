package lnd.sample.service;

import lnd.sample.model.JobTitle;
import lnd.sample.service.base.BasicService;

public interface JobTitleService extends BasicService<JobTitle, Long> {
}

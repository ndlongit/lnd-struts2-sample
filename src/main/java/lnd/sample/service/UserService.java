package lnd.sample.service;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import lnd.sample.misc.search.UserSearch;
import lnd.sample.model.User;
import lnd.sample.service.base.BasicService;

@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
public interface UserService extends BasicService<User, Long> {

    public static final String SERVICE_ID = "userService";

    public List<User> search(UserSearch searchModel);

    public User getByLoginName(String loginName);
}

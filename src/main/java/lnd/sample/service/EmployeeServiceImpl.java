package lnd.sample.service;

import lnd.sample.dao.EmployeeDao;
import lnd.sample.model.Employee;
import lnd.sample.service.base.AbstractService;

import org.springframework.stereotype.Service;

@Service
public class EmployeeServiceImpl extends AbstractService<Employee, Long, EmployeeDao> implements EmployeeService {
}

package lnd.sample.service;

import org.springframework.stereotype.Service;

import lnd.sample.dao.GroupDao;
import lnd.sample.model.Group;
import lnd.sample.service.base.AbstractService;

@Service
public class GroupServiceImpl extends AbstractService<Group, Long, GroupDao> implements GroupService {
}

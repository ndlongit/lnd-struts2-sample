package lnd.sample.service.base;

import static lnd.sample.misc.constant.AppConstants.METHOD_BEGIN;
import static lnd.sample.misc.constant.AppConstants.METHOD_END;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.Priority;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import lnd.sample.dao.base.BasicDao;
import lnd.sample.misc.exception.DataConstraintException;
import lnd.sample.misc.util.AppUtil;
import lnd.sample.model.base.BasicEntity;

@Transactional
@SuppressWarnings({ "unchecked", "rawtypes" })
public abstract class AbstractService<T extends BasicEntity<?>, ID extends Serializable, DAO extends BasicDao> implements BasicService<T, ID> {

    private static Priority logLevel = Level.INFO;

    private static final Logger LOGGER = Logger.getLogger(AbstractService.class);

    protected DAO dao;

    public DAO getDao() {
        return dao;
    }

    // Do not use @Autowired on field directly, otherwise error will appear
    @Autowired
    public void setDao(DAO dao) {
        this.dao = dao;
    }

    @Override
    public T find(ID id) {
        String method = "find(ID id)";
        LOGGER.log(logLevel, startMethod(method));
        T result = (T) dao.find(id);
        LOGGER.log(logLevel, endMethod(method));
        return result;
    }

    @Override
    public List<T> findByIds(List<ID> ids) {
        String method = "findByIds(List<ID> ids)";

        LOGGER.log(logLevel, startMethod(method));
        List results = dao.findByIds(ids);
        LOGGER.log(logLevel, endMethod(method));
        return results;
    }

    @Override
    public List<T> findAll() {
        String method = "findAll()";

        LOGGER.log(logLevel, startMethod(method));
        List<T> results = dao.findAll();
        LOGGER.log(logLevel, endMethod(method));
        return results;
    }

    @Override
    public List<ID> findAllIds() {
        String method = "findAllIds()";

        LOGGER.log(logLevel, startMethod(method));
        List results = dao.findAllIds();
        LOGGER.log(logLevel, endMethod(method));
        return results;
    }

    @Override
    public void delete(ID id) {
        String method = "delete(ID id)";

        LOGGER.log(logLevel, startMethod(method));
        dao.delete(id);
        LOGGER.log(logLevel, endMethod(method));
    }

    @Override
    public int delete(List<ID> ids) {
        String method = "delete(List<ID> ids)";

        int deletedItems = 0;
        LOGGER.log(logLevel, startMethod(method));
        for (ID id : ids) {
            try {
                T entity = (T) dao.find(id);
                delete(entity);
                deletedItems++;
            } catch (Exception e) {
                LOGGER.warn(e.getMessage(), e);
            }
        }

        LOGGER.log(logLevel, endMethod(method));

        return deletedItems;
    }

    @Override
    public void delete(T entity) {
        String method = "delete(T entity)";

        LOGGER.log(logLevel, startMethod(method));
        dao.delete(entity);
        LOGGER.log(logLevel, endMethod(method));
    }

    @Override
    public T save(T entity) throws DataConstraintException, lnd.sample.misc.exception.ValidationException, Exception {
        String method = "save()";

        LOGGER.log(logLevel, startMethod(method));
        try {
            try {
                dao.save(entity);
            } catch (javax.validation.ValidationException e) {
                throw new lnd.sample.misc.exception.ValidationException(e);
            } catch (org.hibernate.exception.ConstraintViolationException e) {
                throw new DataConstraintException(e);
            } catch (javax.persistence.PersistenceException e) {
                throw new DataConstraintException(e);
            } catch (Exception e) {
                throw e;
            }
        } catch (lnd.sample.misc.exception.ValidationException e) {
            throw e;
        } catch (DataConstraintException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw e;
        }

        LOGGER.log(logLevel, endMethod(method));
        return entity;
    }

    @Override
    public T update(T entity) throws DataConstraintException, lnd.sample.misc.exception.ValidationException, Exception {
        String method = "update(T entity)";

        LOGGER.log(logLevel, startMethod(method));
        try {
            try {
                dao.update(entity);
            } catch (javax.validation.ValidationException e) {
                throw new lnd.sample.misc.exception.ValidationException(e);
            } catch (org.hibernate.exception.ConstraintViolationException e) {
                throw new DataConstraintException(e);
            } catch (javax.persistence.PersistenceException e) {
                throw new DataConstraintException(e);
            } catch (Exception e) {
                throw e;
            }
        } catch (lnd.sample.misc.exception.ValidationException e) {
            throw e;
        } catch (DataConstraintException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw e;
        }
        LOGGER.log(logLevel, endMethod(method));
        return entity;
    }

    @Override
    public List<T> updateAll(List<T> entities) throws DataConstraintException, lnd.sample.misc.exception.ValidationException, Exception {
        String method = "updateList(List<T> entity)";

        List<T> ressults = new ArrayList<T>();
        LOGGER.log(logLevel, startMethod(method));

        if (!isNullOrEmpty(entities)) {

            for (T t : entities) {
                try {
                    T entity = update(t);
                    ressults.add(entity);
                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
        }
        LOGGER.log(logLevel, endMethod(method));

        return ressults;
    }

    @Override
    public List<T> findByProperty(String propertyName, List propertyValues) {
        String method = "findByProperty(String propertyName, List propertyValues)";

        LOGGER.log(logLevel, startMethod(method));
        List results = null;
        try {
            results = findByProperty(propertyName, propertyValues, null);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
        LOGGER.log(logLevel, endMethod(method));
        return results;
    }

    @Override
    public List<T> findByProperty(String propertyName, List propertyValues, List<ID> excludeIds) {
        String method = "findByProperty(String propertyName, List propertyValues, List<ID> excludeIds)";

        LOGGER.log(logLevel, startMethod(method));
        List results = null;
        try {
            results = dao.findByProperty(propertyName, propertyValues, excludeIds);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
        LOGGER.log(logLevel, endMethod(method));
        return results;
    }

    @Override
    public T findUniqueByProperty(String propertyName, Object propertyValue) {
        String method = "findUniqueByProperty()";

        LOGGER.log(logLevel, startMethod(method));
        T result = null;
        try {
            result = (T) dao.findUniqueByProperty(propertyName, propertyValue);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
        LOGGER.log(logLevel, endMethod(method));
        return result;
    }

    protected static boolean isNullOrEmpty(Object value) {
        return AppUtil.isNullOrEmpty(value);
    }

    protected static String startMethod(String method) {
        return method + METHOD_BEGIN;
    }

    protected static String endMethod(String method) {
        return method + METHOD_END;
    }
}

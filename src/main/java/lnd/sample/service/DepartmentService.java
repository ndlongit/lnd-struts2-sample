package lnd.sample.service;

import lnd.sample.model.Department;
import lnd.sample.service.base.BasicService;

public interface DepartmentService extends BasicService<Department, Long> {
}

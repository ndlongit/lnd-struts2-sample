package lnd.sample.service;

import java.util.List;

import lnd.sample.model.Role;
import lnd.sample.service.base.BasicService;

public interface RoleService extends BasicService<Role, Long> {

    public List<Role> getByNames(List<String> roleNames);
}

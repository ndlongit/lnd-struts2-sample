package lnd.sample.service;

import lnd.sample.dao.DepartmentDao;
import lnd.sample.model.Department;
import lnd.sample.service.base.AbstractService;

import org.springframework.stereotype.Service;

@Service
public class DepartmentServiceImpl extends AbstractService<Department, Long, DepartmentDao> implements DepartmentService {
}

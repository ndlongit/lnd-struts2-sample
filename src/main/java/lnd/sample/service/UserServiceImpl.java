package lnd.sample.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lnd.sample.component.IStringEncryption;
import lnd.sample.dao.UserDao;
import lnd.sample.misc.exception.DataConstraintException;
import lnd.sample.misc.exception.ValidationException;
import lnd.sample.misc.search.UserSearch;
import lnd.sample.model.User;
import lnd.sample.service.base.AbstractService;

@Service(UserService.SERVICE_ID)
public class UserServiceImpl extends AbstractService<User, Long, UserDao> implements UserService {

    @Autowired(required = false)
    private IStringEncryption passwordEncoder;

    @Override
    public List<User> search(UserSearch searchModel) {
        List<User> results = dao.search(searchModel);
        return results;
    }

    @Override
    public User getByLoginName(String loginName) {
        return dao.getByLoginName(loginName);
    }

    @Override
    public User save(User entity) throws DataConstraintException, ValidationException, Exception {
        enscryptPassword(entity, passwordEncoder);

        return super.save(entity);
    }

    @Override
    public User update(User entity) throws DataConstraintException, ValidationException, Exception {
        enscryptPassword(entity, passwordEncoder);

        return super.update(entity);
    }

    private static void enscryptPassword(User entity, IStringEncryption encoder) {
        String password = entity.getPassword();
        if (isNullOrEmpty(password)) {
            return;
        }

        if (encoder == null) {
            return;
        }

        String encryptedPassword = encoder.encode(password);
        entity.setEncryptedPassword(encryptedPassword);
    }
}

package lnd.sample.dao;

import lnd.sample.dao.base.BasicDao;
import lnd.sample.model.Group;

public interface GroupDao extends BasicDao<Group, Long> {
}

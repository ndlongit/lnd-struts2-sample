package lnd.sample.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import lnd.sample.dao.base.AbstractDao;
import lnd.sample.model.Role;
import lnd.sample.model.base.SimpleEntity;

@Repository
public class RoleDaoImpl extends AbstractDao<Role, Long> implements RoleDao {

    @Override
    public List<Role> getByNames(List<String> roleNames) {
        return super.findByProperty(SimpleEntity.PROP_NAME, roleNames);
    }
}

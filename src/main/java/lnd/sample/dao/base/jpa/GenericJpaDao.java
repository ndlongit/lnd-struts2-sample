package lnd.sample.dao.base.jpa;

import static lnd.sample.misc.constant.AppConstants.METHOD_BEGIN;
import static lnd.sample.misc.constant.AppConstants.METHOD_END;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.Priority;

import lnd.sample.misc.util.AppUtil;
import lnd.sample.model.base.BasicEntity;
import lnd.sample.model.base.LogicalDeletion;
import lnd.sample.model.base.NumericIdEntity;
import lnd.sample.model.base.Orderable;

public abstract class GenericJpaDao<T extends BasicEntity<?>, ID extends Serializable> {

    private static Priority logLevel = Level.DEBUG;

    private static final Logger LOGGER = Logger.getLogger(GenericJpaDao.class);

    protected EntityManager entityManager;

    private final Class<T> clazz;

    @SuppressWarnings(value = "unchecked")
    protected GenericJpaDao() {
        // Note: This depends on Class hierarchy (extends)
        Type genericSuperclass = getClass().getGenericSuperclass();
        ParameterizedType parameterizedType = (ParameterizedType) genericSuperclass;
        this.clazz = (Class<T>) parameterizedType.getActualTypeArguments()[0];
    }

    @PersistenceContext
    public final void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public Class<T> getClazz() {
        return clazz;
    }

    public void save(final T entity) throws javax.validation.ValidationException, javax.persistence.PersistenceException,
            org.hibernate.exception.ConstraintViolationException, Exception {

        String method = "save()";

        LOGGER.log(logLevel, startMethod(method));
        if (entity instanceof NumericIdEntity) { /* Auto-increment ID, so clear ID before saving */
            entity.setId(null);
        }
        this.entityManager.persist(entity);
        LOGGER.log(logLevel, endMethod(method));
    }

    public T update(T entity) throws javax.validation.ValidationException, javax.persistence.PersistenceException,
            org.hibernate.exception.ConstraintViolationException, Exception {
        String method = "save()";

        LOGGER.log(logLevel, startMethod(method));
        T result = this.entityManager.merge(entity);
        LOGGER.log(logLevel, "update()" + METHOD_END);

        return result;
    }

    public void delete(ID id) throws UnsupportedOperationException, IllegalArgumentException, javax.persistence.TransactionRequiredException {
        String method = "delete(ID id)";

        LOGGER.log(logLevel, startMethod(method));
        this.delete(find(id));
        LOGGER.log(logLevel, endMethod(method));
    }

    public void delete(T entity) throws UnsupportedOperationException, IllegalArgumentException, javax.persistence.TransactionRequiredException {

        String method = "delete(T entity)";

        LOGGER.log(logLevel, startMethod(method));
        if (isLogicalDeletion(entity)) {
            ((LogicalDeletion) entity).setDeleted(true);
            try {
                update(entity);
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
            }
        } else {
            this.entityManager.remove(entity);
        }
        LOGGER.log(logLevel, endMethod(method));
    }

    public T find(ID id) {

        String method = "find(ID id)";

        LOGGER.log(logLevel, startMethod(method));
        T result = this.entityManager.find(this.clazz, id);
        result = filterDeletedItem(result);
        LOGGER.log(logLevel, endMethod(method));
        return result;
    }

    /** Return NULL if the entity is marked as deleted */
    protected T filterDeletedItem(T entity) {
        if (isLogicalDeletion(entity)) {
            if (((LogicalDeletion) entity).isDeleted() == true) {
                return null;
            }
        }

        return entity;
    }

    public List<T> findByIds(List<T> ids) {

        String method = "findByIds(List<T> ids)";

        LOGGER.log(logLevel, startMethod(method));
        List<T> results = findByProperty(BasicEntity.PROP_ID, ids, null);
        LOGGER.log(logLevel, endMethod(method));
        return results;
    }

    @SuppressWarnings({ "unchecked" })
    public List<T> findAll() {

        String method = "findAll()";

        LOGGER.log(logLevel, startMethod(method));
        String queryString = getFrom();
        queryString += buildOrderByClause();
        Query queryObject = this.entityManager.createQuery(queryString);
        List<T> results = queryObject.getResultList();
        LOGGER.log(logLevel, endMethod(method));
        return results;
    }

    @SuppressWarnings({ "unchecked" })
    public List<ID> findAllIds() {

        String method = "findAllIds()";

        LOGGER.log(logLevel, startMethod(method));
        String queryString = "SELECT " + BasicEntity.PROP_ID + " " + getFrom();
        queryString += buildOrderByClause();
        Query queryObject = this.entityManager.createQuery(queryString);
        List<ID> results = queryObject.getResultList();
        LOGGER.log(logLevel, endMethod(method));
        return results;
    }

    @SuppressWarnings("unchecked")
    public T findUniqueByProperty(String propertyName, Object propertyValue) throws NoResultException, NonUniqueResultException {

        String method = "findUniqueByProperty()";

        LOGGER.log(logLevel, startMethod(method));

        T result = null;
        if (propertyValue != null) {
            String queryString = getFrom() + " AND " + propertyName + " = ?1";
            Query queryObject = this.entityManager.createQuery(queryString);
            queryObject.setParameter(1, propertyValue);

            result = (T) queryObject.getSingleResult();
        }

        LOGGER.log(logLevel, endMethod(method));
        return result;
    }

    public List<T> findByProperty(String propertyName, List<?> propertyValues) {
        return findByProperty(propertyName, propertyValues, null);
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public List<T> findByProperty(String propertyName, List<?> propertyValues, List<ID> excludeIds) {

        String method = "findByProperty()";

        LOGGER.log(logLevel, startMethod(method));
        StringBuilder sb = new StringBuilder();
        sb.append(getFrom());
        sb.append(" AND ");
        sb.append(propertyName);
        sb.append(" IN (?1)");

        if (!isNullOrEmpty(excludeIds)) {
            sb.append(" AND ID NOT IN (?2)");
        }

        sb.append(buildOrderByClause());
        // String queryString = getFrom() + " AND " + propertyName + " IN (?1)";
        // queryString += buildOrderByClause();

        Query queryObject = this.entityManager.createQuery(sb.toString());
        queryObject.setParameter(1, propertyValues);
        if (!isNullOrEmpty(excludeIds)) {
            queryObject.setParameter(2, excludeIds);
        }

        List results = queryObject.getResultList();
        LOGGER.log(logLevel, endMethod(method));
        return results;
    }

    protected String buildOrderByClause() {
        try {
            T entity = createInstance();
            if (entity instanceof Orderable) {
                return ((Orderable) entity).getOrderByClause();
            }

            return "";
        }

        catch (Exception e) {
            LOGGER.warn(e.getMessage(), e);
            return "";
        }
    }

    protected String getFrom() {
        String sql = "FROM " + getClazz().getName();
        String condition = null;
        try {
            if (isLogicalDeletion(createInstance())) {
                condition = LogicalDeletion.NOT_DELETED;
            } else {
                condition = "1 = 1";
            }
            sql += " WHERE " + condition;
        } catch (Exception e) {
            LOGGER.warn(e.getMessage(), e);
        }
        return sql;
    }

    private static String startMethod(String method) {
        return method + METHOD_BEGIN;
    }

    private static String endMethod(String method) {
        return method + METHOD_END;
    }

    protected static boolean isNullOrEmpty(final Object value) {
        return AppUtil.isNullOrEmpty(value);
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    protected static Predicate buildLikeExpression(final CriteriaBuilder cb, Path path, final String value) {
        String pattern = null;
        if (!isNullOrEmpty(value)) {
            pattern = "%" + value.trim().toUpperCase() + "%";
        }

        return cb.like(cb.upper(path), pattern);
    }

    protected static void addLikeExpression(final List<Predicate> conditions, final CriteriaBuilder cb, final Path<?> path, String value) {
        if (conditions == null) {
            return;
        }

        if (!isNullOrEmpty(value)) {
            conditions.add(buildLikeExpression(cb, path, value));
        }
    }

    public Collection<? extends Predicate> getDefaultConditions(Root<?> rootEntity, CriteriaBuilder cb) {
        ArrayList<Predicate> defaultConditions = new ArrayList<Predicate>();
        if (isLogicalDeletion(createInstance())) {
            Predicate notDeleted = cb.equal(rootEntity.get(LogicalDeletion.PROP_DELETED), false);
            defaultConditions.add(notDeleted);
        }
        return defaultConditions;
    }

    protected T createInstance() {
        try {
            return getClazz().newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }
    }

    protected boolean isLogicalDeletion(T bean) {
        boolean result = bean instanceof LogicalDeletion;
        return result;
    }
}

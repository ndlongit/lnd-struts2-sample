package lnd.sample.dao.base;

import java.io.Serializable;

import lnd.sample.dao.base.jpa.GenericJpaDao;
import lnd.sample.model.base.BasicEntity;

public abstract class AbstractDao<T extends BasicEntity<?>, ID extends Serializable> extends GenericJpaDao<T, ID> implements BasicDao<T, ID> {
}

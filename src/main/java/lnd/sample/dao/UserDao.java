package lnd.sample.dao;

import java.util.List;

import lnd.sample.dao.base.BasicDao;
import lnd.sample.misc.search.UserSearch;
import lnd.sample.model.User;

public interface UserDao extends BasicDao<User, Long> {

    User getByLoginName(final String loginName);

    List<User> search(UserSearch searchModel);

}

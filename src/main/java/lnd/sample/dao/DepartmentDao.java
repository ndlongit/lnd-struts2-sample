package lnd.sample.dao;

import lnd.sample.dao.base.BasicDao;
import lnd.sample.model.Department;

public interface DepartmentDao extends BasicDao<Department, Long> {
}

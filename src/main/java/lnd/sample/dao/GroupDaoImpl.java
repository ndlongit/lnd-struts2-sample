package lnd.sample.dao;

import org.springframework.stereotype.Repository;

import lnd.sample.dao.base.AbstractDao;
import lnd.sample.model.Group;

@Repository
public class GroupDaoImpl extends AbstractDao<Group, Long> implements GroupDao {
}

package lnd.sample.dao;

import lnd.sample.dao.base.AbstractDao;
import lnd.sample.model.Department;

import org.springframework.stereotype.Repository;

@Repository
public class DepartmentDaoImpl extends AbstractDao<Department, Long> implements DepartmentDao {
}

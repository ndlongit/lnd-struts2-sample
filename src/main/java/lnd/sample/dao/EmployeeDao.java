package lnd.sample.dao;

import lnd.sample.dao.base.BasicDao;
import lnd.sample.model.Employee;

public interface EmployeeDao extends BasicDao<Employee, Long> {
}

package lnd.sample.dao;

import lnd.sample.dao.base.BasicDao;
import lnd.sample.model.JobTitle;

public interface JobTitleDao extends BasicDao<JobTitle, Long> {
}

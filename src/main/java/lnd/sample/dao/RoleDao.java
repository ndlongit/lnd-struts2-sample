package lnd.sample.dao;

import java.util.List;

import lnd.sample.dao.base.BasicDao;
import lnd.sample.model.Role;

public interface RoleDao extends BasicDao<Role, Long> {

    List<Role> getByNames(List<String> roleNames);
}

package lnd.sample.dao;

import lnd.sample.dao.base.AbstractDao;
import lnd.sample.model.Employee;

import org.springframework.stereotype.Repository;

@Repository
public class EmployeeDaoImpl extends AbstractDao<Employee, Long> implements EmployeeDao {
}

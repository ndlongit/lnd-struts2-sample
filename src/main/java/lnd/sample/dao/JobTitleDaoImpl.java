package lnd.sample.dao;

import lnd.sample.dao.base.AbstractDao;
import lnd.sample.model.JobTitle;

import org.springframework.stereotype.Repository;

@Repository
public class JobTitleDaoImpl extends AbstractDao<JobTitle, Long> implements JobTitleDao {
}

package lnd.sample.web.action;

import lnd.sample.web.action.base.AbstractAction;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Actions;
import org.apache.struts2.convention.annotation.Result;

@Actions(value = { @Action("/"), @Action("index"), @Action("trang-chu") })
@Result(location = "/index.jsp")
public class HomeAction extends AbstractAction {
}

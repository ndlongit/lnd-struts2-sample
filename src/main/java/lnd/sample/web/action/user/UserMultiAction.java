package lnd.sample.web.action.user;

import java.util.List;

import lnd.sample.model.Group;
import lnd.sample.model.User;
import lnd.sample.service.GroupService;
import lnd.sample.service.UserService;
import lnd.sample.web.action.base.AbstractAction;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.springframework.beans.factory.annotation.Autowired;

@Results({ @Result(name = AbstractAction.ACTION_LIST, location = AbstractAction.ACTION_LIST, type = AbstractAction.TYPE_REDIRECT_ACTION),
        @Result(name = AbstractAction.COPY_MULTIPLE, location = "copyMultiUsers.jsp"),
        @Result(name = AbstractAction.EDIT_MULTIPLE, location = "editMultiUsers.jsp"),
        @Result(name = com.opensymphony.xwork2.Action.INPUT, location = "editMultiUsers.jsp") })
public class UserMultiAction extends AbstractAction {

    @Autowired
    private UserService userService;
    @Autowired
    private GroupService groupService;

    private List<User> results;
    private List<Group> groupModels;
    private List<Long> ids;

    public String actionMulti;

    public List<User> getResults() {
        return results;
    }

    public void setResults(List<User> results) {
        this.results = results;
    }

    public List<Long> getIds() {
        return ids;
    }

    public void setIds(List<Long> ids) {
        this.ids = ids;
    }

    public List<Group> getGroupModels() {
        return groupModels;
    }

    public void setGroupModels(List<Group> groupModels) {
        this.groupModels = groupModels;
    }

    @SkipValidation
    @Action(ACTION_PRE_EDIT_MULTI)
    public String preUpdateMulti() {
        try {
            actionMulti = ACTION_EDIT_MULTI;
            this.groupModels = groupService.findAll();
            this.results = userService.findByIds(ids);
            return EDIT_MULTIPLE;
        } catch (Exception e) {
            return ERROR;
        }
    }

    @SkipValidation
    @Action(ACTION_EDIT_MULTI)
    public String updateMulti() {
        try {
            if (!isNullOrEmpty(results)) {
                for (User user : results) {
                    try {
                        userService.update(user);
                    } catch (Exception e) {
                        logger.error(e);
                    }
                }
            }

            return ACTION_LIST;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return ERROR;
        }
    }

    @SkipValidation
    @Action(ACTION_PRE_COPY_MULTI)
    public String preCopyMulti() {
        try {
            actionMulti = ACTION_COPY_MULTI;
            this.groupModels = groupService.findAll();
            this.results = userService.findByIds(ids);
            if (!isNullOrEmpty(this.results)) {
                for (User user : results) {
                    if (user != null) {
                        user.setLoginName(user.getLoginName() + "-Copy");
                    }
                }
            }
            return COPY_MULTIPLE;
        } catch (Exception e) {
            return ERROR;
        }
    }

    @SkipValidation
    @Action(ACTION_COPY_MULTI)
    public String copyMulti() {
        try {
            if (!isNullOrEmpty(results)) {
                for (User user : results) {
                    try {
                        userService.save(user);
                    } catch (Exception e) {
                        logger.error(e);
                    }
                }
            }

            return ACTION_LIST;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return ERROR;
        }
    }
}

package lnd.sample.web.action.user;

import java.util.List;

import lnd.sample.misc.exception.DataConstraintException;
import lnd.sample.misc.search.UserSearch;
import lnd.sample.model.User;
import lnd.sample.service.UserService;
import lnd.sample.web.action.base.AbstractAction;
import lnd.sample.web.action.base.SearchAction;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ModelDriven;

@Results({ @Result(name = AbstractAction.SEARCH, location = UserSearchAction.MAIN_PAGE),
        @Result(name = com.opensymphony.xwork2.Action.ERROR, location = UserSearchAction.MAIN_PAGE),
        @Result(name = com.opensymphony.xwork2.Action.INPUT, location = UserSearchAction.MAIN_PAGE),
        @Result(name = AbstractAction.GRID, location = "UsersGrid.jsp") })
public class UserSearchAction extends SearchAction implements ModelDriven<UserSearch> {

    protected static final String MAIN_PAGE = "searchUsers.jsp";

    @Autowired
    private UserService userService;
    private List<User> results;
    private String headerText;
    private UserSearch search;

    public List<User> getResults() {
        return results;
    }

    public void setResults(List<User> results) {
        this.results = results;
    }

    public UserSearch getSearch() {
        return search;
    }

    public void setSearch(UserSearch search) {
        this.search = search;
    }

    // @SkipValidation
    // @Action(value = ACTION_PRE_SEARCH, results = { @Result(name = PREPARE, location = "searchUsers.jsp") })
    // public String preSearch() throws DataConstraintException, Exception {
    // try {
    // initDataForSearch();
    // return PREPARE;
    // } catch (Exception e) {
    // addActionError("Prepare data fail");
    // initDataForSearch();
    // return ERROR;
    // }
    // }

    @Action(value = ACTION_SEARCH, results = { @Result(name = PREPARE, location = MAIN_PAGE) })
    public String search() throws DataConstraintException, Exception {
        try {
            if (!submitted) {
                initDataForSearch();
                return PREPARE;
            } else {
                this.results = userService.search(search);
                return GRID;
            }
        } catch (Exception e) {
            addActionError("Prepare data fail");
            initDataForSearch();
            return ERROR;
        }
    }

    private void initDataForSearch() {
    }

    public String getHeaderText() {
        return headerText;
    }

    public void setHeaderText(String headerText) {
        this.headerText = headerText;
    }

    @Override
    public void prepare() throws Exception {
        search = new UserSearch();
    }

    @Override
    public void validate() {
    }

    @Override
    public UserSearch getModel() {
        return this.search;
    }
}

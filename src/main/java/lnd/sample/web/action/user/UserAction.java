package lnd.sample.web.action.user;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import lnd.sample.misc.exception.DataConstraintException;
import lnd.sample.model.Group;
import lnd.sample.model.Role;
import lnd.sample.model.User;
import lnd.sample.service.GroupService;
import lnd.sample.service.RoleService;
import lnd.sample.service.UserService;
import lnd.sample.web.action.base.AbstractAction;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ModelDriven;

@Namespace(value = UserAction.NAME_SPACE)
@Results({ @Result(name = AbstractAction.LIST, location = "listUsers.jsp"),
        @Result(name = com.opensymphony.xwork2.Action.ERROR, location = "createUser.jsp"),
        @Result(name = AbstractAction.GRID, location = "UsersGrid.jsp"),
        @Result(name = com.opensymphony.xwork2.Action.INPUT, location = "createUser.jsp") })
public class UserAction extends AbstractAction implements ModelDriven<User> {

    static final String NAME_SPACE = "/user";

    @Autowired
    private UserService userService;
    @Autowired
    private GroupService groupService;
    @Autowired
    private RoleService roleService;

    private List<Group> groupModels;
    private List<Long> groupValues;
    private List<Role> roleModels;
    private List<Long> roleValues;

    private List<User> results;

    @Valid
    private User user = new User();

    private String password2;
    private String headerText;
    private List<Long> ids;

    public List<Long> getRoleValues() {
        return roleValues;
    }

    public void setRoleValues(List<Long> roleValues) {
        this.roleValues = roleValues;
    }

    @SkipValidation
    @Action(value = ACTION_LIST)
    public String list() {
        setResults(userService.findAll());

        // For test
        if (test) {
            setResults(null);
        }
        return LIST;
    }

    @SkipValidation
    @Action(value = ACTION_PRE_CREATE, results = { @Result(name = PREPARE, location = "createUser.jsp") })
    public String create() throws DataConstraintException, Exception {
        try {
            initDataForCreate();
            return PREPARE;
        } catch (Exception e) {
            addActionError("Prepare data fail: " + e.getMessage());
            initDataForCreate();
            return ERROR;
        }
    }

    @Action(value = ACTION_CREATE)
    public String doCreate() throws DataConstraintException, Exception {
        try {
            try {
                setCommonProperties();
            } catch (Exception e) {
            }
            this.userService.save(user);
            return ACTION_LIST;
        } catch (Exception e) {
            addActionError("Create User fail: " + e.getMessage());
            initDataForCreate();
            return ERROR;
        }
    }

    @SkipValidation
    @Action(value = ACTION_PRE_COPY, results = { @Result(name = PREPARE, location = "createUser.jsp") })
    public String copy() throws DataConstraintException, Exception {
        try {
            initDataForCopy();
            loadDataModel(user);

            if (user == null) {
                user = new User();
            } else {

                // Clear all un-copytable fields
                user.setLoginName(user.getLoginName() + "-Copy");
                user.setPassword(null);
                password2 = null;
            }

            return PREPARE;
        } catch (Exception e) {
            addActionError("Prepare data fail: " + e.getMessage());
            initDataForCreate();
            return ERROR;
        }
    }

    @Action(value = ACTION_COPY, results = { @Result(name = ACTION_VIEW, location = ACTION_VIEW, type = AbstractAction.TYPE_REDIRECT_ACTION, params = {
            "id", "%{id}" }) })
    public String doCopy() throws DataConstraintException, Exception {
        try {
            try {
                setCommonProperties();
            } catch (Exception e) {
            }

            this.userService.save(user);

            pushModel(this.user);
            return ACTION_VIEW;
        } catch (Exception e) {
            addActionError("Copy User fail: " + e.getMessage());
            initDataForCreate();
            return PREPARE;
        }
    }

    @SkipValidation
    @Action(value = ACTION_PRE_EDIT, results = { @Result(name = PREPARE, location = "createUser.jsp") })
    public String edit() {
        try {
            initDataForEdit();
            loadDataModel(user);

            return PREPARE;
        } catch (Exception e) {
            addActionMessage(e.getMessage());
            initDataForEdit();
            return PREPARE;
        }
    }

    @Action(value = ACTION_EDIT)
    public String doEdit() {
        try {
            try {
                setCommonProperties();
            } catch (Exception e) {
            }

            this.userService.update(user);
            return ACTION_LIST;
        } catch (Exception e) {
            addActionMessage(e.getMessage());
            initDataForEdit();
            return ERROR;
        }
    }

    private void setCommonProperties() {

        if (!isNullOrEmpty(groupValues)) {
            List<Group> groups = new ArrayList<Group>();
            for (Long groupId : groupValues) {
                groups.add(new lnd.sample.model.Group(groupId));
            }
            user.setGroups(groups);
        }

        if (!isNullOrEmpty(roleValues)) {
            List<Role> roles = new ArrayList<Role>();
            for (Long roleId : roleValues) {
                roles.add(new Role(roleId));
            }
            user.setRoles(roles);
        }

    }

    @SkipValidation
    @Action(value = ACTION_VIEW, results = { @Result(name = SUCCESS, location = "viewUser.jsp") })
    public String view() {
        try {
            pageTitle = "View User Detail";
            headerText = pageTitle;
            loadDataModel(user);
            return SUCCESS;
        } catch (Exception e) {
            // Add errors
            return SUCCESS;
        }
    }

    private void initDataForCreate() {
        action = actionCreate;
        pageTitle = "Create New User";
        headerText = pageTitle;
        initCommonData();
    }

    private void initDataForCopy() {
        action = ACTION_COPY;
        pageTitle = "Copy User";
        headerText = pageTitle;
        initCommonData();
    }

    private void initDataForEdit() {
        action = ACTION_EDIT;
        pageTitle = "Edit User";
        headerText = pageTitle;
        initCommonData();
    }

    private void initCommonData() {
        this.groupModels = groupService.findAll();
        this.roleModels = roleService.findAll();
    }

    @SkipValidation
    @Action(ACTION_DELETE)
    public String delete() {
        try {
            if (user != null && !isNullOrEmpty(user.getId())) {
                // userService.delete(user.getId());
            }

            setResults(userService.findAll());
            return GRID;
        } catch (Exception e) {
            handleAjaxException("[Ajax]: Delete User un-successfully");
            return null;
        }
    }

    @SkipValidation
    @Action(ACTION_DELETE_MULTI)
    public String deleteMultiple() {
        try {
            if (!isNullOrEmpty(ids)) {
                userService.delete(ids);
            }

            setResults(userService.findAll());
            return GRID;
        } catch (Exception e) {
            return ERROR;
        }
    }

    public List<User> getResults() {
        return results;
    }

    public void setResults(List<User> results) {
        this.results = results;
    }

    @Override
    public void prepare() throws Exception {
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getPassword2() {
        return password2;
    }

    public void setPassword2(String password2) {
        this.password2 = password2;
    }

    public String getHeaderText() {
        return headerText;
    }

    public void setHeaderText(String headerText) {
        this.headerText = headerText;
    }

    public List<Long> getIds() {
        return ids;
    }

    public void setIds(List<Long> ids) {
        this.ids = ids;
    }

    @Override
    public void validate() {
        // if (ACTION_DO_CREATE.equalsIgnoreCase(action) || ACTION_DO_EDIT.equalsIgnoreCase(action)) {
        // final User model = getModel();
        // String[] fieldValues = { model.getLoginName(), model.getPassword(), password2, model.getFirstName(), model.getLastName() };
        // String[] fieldNames = { "loginName", "password", "password2", "firstName", "lastName" };
        // String[] fieldLabels = { getText("loginName"), getText("password"), getText("password2"), getText("firstName"), getText("lastName") };
        // validateRequired(fieldValues, fieldLabels, fieldNames);
        // }

        if (ACTION_CREATE.equalsIgnoreCase(action)) {
            initDataForCreate();
        } else if (ACTION_EDIT.equalsIgnoreCase(action)) {
            initDataForEdit();
        } else {
            initDataForCopy();
        }
    }

    private void loadDataModel(final User model) {
        if (model != null && model.getId() != null) {
            this.user = userService.find(model.getId());
            if (this.user != null) {

                try {
                    initGroupValues();
                    initRoleValues();

                    // init other data lists here
                } catch (Exception e) {
                }

                pushModel(this.user);
            }
        }
    }

    private void initGroupValues() {
        List<Group> assignedGroups = this.user.getGroups();
        if (!isNullOrEmpty(assignedGroups)) {
            groupValues = new ArrayList<Long>();

            for (Group group : assignedGroups) {
                groupValues.add(group.getId());
            }
        }
    }

    private void initRoleValues() {
        List<Role> assignedRoles = this.user.getRoles();
        if (!isNullOrEmpty(assignedRoles)) {
            roleValues = new ArrayList<Long>();

            for (Role role : assignedRoles) {
                roleValues.add(role.getId());
            }
        }
    }

    public List<Group> getGroupModels() {
        return groupModels;
    }

    public void setGroupModels(List<Group> groupModels) {
        this.groupModels = groupModels;
    }

    public List<Role> getRoleModels() {
        return roleModels;
    }

    public void setRoleModels(List<Role> roleModels) {
        this.roleModels = roleModels;
    }

    public List<Long> getGroupValues() {
        return groupValues;
    }

    public void setGroupValues(List<Long> groupValues) {
        this.groupValues = groupValues;
    }

    @Override
    public User getModel() {
        return this.user;
    }
}

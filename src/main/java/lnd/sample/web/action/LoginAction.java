package lnd.sample.web.action;

import lnd.sample.web.action.base.AbstractAction;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

@Results({ @Result(location = "login-success.jsp"), @Result(name = com.opensymphony.xwork2.Action.LOGIN, location = "/login.jsp") })
public class LoginAction extends AbstractAction {

    @Override
    public String execute() throws Exception {
        return com.opensymphony.xwork2.Action.LOGIN;
    }

    @Action("login-success")
    public String loginSuccess() throws Exception {
        return SUCCESS;
    }

    @Action("login-fail")
    public String loginFail() throws Exception {
        addActionError(getText("login.fail"));
        return com.opensymphony.xwork2.Action.LOGIN;
    }

    @Action("access-denied")
    public String accessDenied() throws Exception {
        addActionError(getText("login.accessDenied"));
        return com.opensymphony.xwork2.Action.LOGIN;
    }
}

package lnd.sample.web.action.base;

import java.util.Arrays;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.validation.SkipValidation;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;

import lnd.sample.misc.util.AppUtil;

// Global Results
@Results({ @Result(name = AbstractAction.ACTION_LIST, location = AbstractAction.ACTION_LIST, type = AbstractAction.TYPE_REDIRECT_ACTION) })
public abstract class AbstractAction extends ActionSupport implements Preparable {

    protected boolean submitted;

    private static final String PARAM_NAMESPACE = "namespace";

    public static final String HTTP_HEADER_ERROR = "errorMessage"; // HTTP Header name to store error message (Used for Ajax call)

    private static final long serialVersionUID = 1L;
    protected transient Logger logger = Logger.getLogger(this.getClass().getName());

    protected HttpServletRequest request = ServletActionContext.getRequest();
    protected HttpServletResponse response = ServletActionContext.getResponse();
    protected HttpSession session = request.getSession();

    /* Actions - Begin */
    public static final String ACTION_HOME = "home";
    private static final String ACTION_MULTIPLE = "Multiple";
    public static final String ACTION_PRE_CREATE = "preCreate";
    public static final String ACTION_CREATE = "create";
    public static final String ACTION_PRE_COPY = "preCopy";
    public static final String ACTION_COPY = "copy";
    public static final String ACTION_PRE_EDIT = "preEdit";
    public static final String ACTION_EDIT = "edit";
    public static final String ACTION_VIEW = "view";
    public static final String ACTION_LIST = "list";
    public static final String ACTION_PRE_SEARCH = "preSearch";
    public static final String ACTION_SEARCH = "search";
    public static final String ACTION_DELETE = "delete";

    public static final String ACTION_PRE_COPY_MULTI = ACTION_PRE_COPY + ACTION_MULTIPLE;
    public static final String ACTION_COPY_MULTI = ACTION_COPY + ACTION_MULTIPLE;
    public static final String ACTION_PRE_EDIT_MULTI = ACTION_PRE_EDIT + ACTION_MULTIPLE;
    public static final String ACTION_EDIT_MULTI = ACTION_EDIT + ACTION_MULTIPLE;
    public static final String ACTION_DELETE_MULTI = ACTION_DELETE + ACTION_MULTIPLE;
    /* Actions - End */

    /* Views/Locations - Begin */
    public static final String HOME = "home";
    public static final String PREPARE = "prepareView";
    public static final String CREATE = "createView";
    public static final String COPY = "copyView";
    public static final String EDIT = "editView";
    public static final String LIST = "listView";

    /* This is used for Ajax call, in the case only return a result list, not whole page */
    public static final String GRID = "gridView";
    public static final String SEARCH = "searchView";
    public static final String VIEW = "viewView";
    public static final String COPY_MULTIPLE = "copyMultiView";
    public static final String EDIT_MULTIPLE = "editMultiView";
    /* Views/Locations - End */

    public static final String VALIDATION_REQUIRED = "validation.required";
    public static final String VALIDATION_INVALID = "validation.invalid";
    public static final String TYPE_REDIRECT_ACTION = "redirectAction";

    protected String action;

    // /** Action modes: {PREPARE | CREATE | COPY | LIST | EDIT | VIEW | DELETE} */
    // protected String mode = PREPARE;

    // Properties for Constants - Begin
    public String actionPreCreate = ACTION_PRE_CREATE;
    public String actionPreCopy = ACTION_PRE_COPY;
    public String actionPreEdit = ACTION_PRE_EDIT;
    public String actionView = ACTION_VIEW;
    public String actionCreate = ACTION_CREATE;
    public String actionCopy = ACTION_COPY;
    public String actionEdit = ACTION_EDIT;
    public String actionDelete = ACTION_DELETE;

    public String actionPreCopyMulti = ACTION_PRE_COPY_MULTI;
    public String actionCopyMulti = ACTION_COPY_MULTI;
    public String actionPreEditMulti = ACTION_PRE_EDIT_MULTI;
    public String actionEditMulti = ACTION_EDIT_MULTI;
    public String actionDeleteMulti = ACTION_DELETE_MULTI;
    // Properties for Constants - End

    public String languageCode;

    protected static final String LOCALE_PARAM = "WW_TRANS_I18N_LOCALE";

    protected String pageTitle;

    // For test - Begin
    public boolean test;

    // For test - End

    protected AbstractAction() {
        Locale l = (Locale) session.getAttribute(LOCALE_PARAM);
        if (l != null) {
            languageCode = l.getLanguage();
        }
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getPageTitle() {
        return pageTitle;
    }

    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }

    protected static boolean isNullOrEmpty(Object value) {
        return AppUtil.isNullOrEmpty(value);
    }

    protected void validateRequired(String[] fieldValues, String[] fieldLabels, String[] fieldNames) {
        if (isNullOrEmpty(fieldValues) || isNullOrEmpty(fieldLabels)) {
            return;
        }

        String fieldName = null;
        int length = Math.min(fieldValues.length, fieldLabels.length);
        for (int i = 0; i < length; i++) {
            if (isNullOrEmpty(fieldValues[i])) {
                if (!isNullOrEmpty(fieldNames) && fieldNames.length > i) {

                    // Also add Field Error
                    fieldName = fieldNames[i];
                }

                addError(getText(VALIDATION_REQUIRED, Arrays.asList(fieldLabels[i])), fieldName);
            }
        }
    }

    /**
     * Add error for Action only, not for FormFields
     * 
     * @param message
     */
    protected void addError(String message) {
        this.addError(message, (String) null);
    }

    /**
     * Add error for Action or FormFields
     * 
     * @param message
     * @param fieldName
     */
    protected void addError(String message, String fieldName) {
        if (isNullOrEmpty(fieldName)) {
            addActionError(message);
        } else {
            addFieldError(fieldName, message);
        }
    }

    @Override
    public void prepare() throws Exception {

        // Sub-classes should implement this method as needed
    }

    @Override
    public void validate() {

        // Sub-classes should implement this method as needed
    }

    // Push Object into ValueStack for references from JSP files
    protected static void pushModel(Object o) {
        ActionContext.getContext().getValueStack().push(o);
    }

    protected void handleAjaxException(String headerMessage) {
        try {
            response.setHeader(HTTP_HEADER_ERROR, headerMessage);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    @Action(value = ACTION_HOME, results = { @Result(name = HOME, location = "/index.jsp") })
    @SkipValidation
    public String home() {
        return HOME;
    }

    @Action(value = "changeLanguage", results = { @Result(name = ACTION_HOME, location = ACTION_HOME, type = AbstractAction.TYPE_REDIRECT_ACTION, params = {
            PARAM_NAMESPACE, "/" }) })
    @SkipValidation
    public String changeLanguage() {
        if (!isNullOrEmpty(languageCode)) {
            session.setAttribute(LOCALE_PARAM, new Locale(languageCode));
        }

        return ACTION_HOME;
    }

    public boolean isSubmitted() {
        return submitted;
    }

    public void setSubmitted(boolean submitted) {
        this.submitted = submitted;
    }
}

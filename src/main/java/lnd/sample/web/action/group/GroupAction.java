package lnd.sample.web.action.group;

import java.util.List;

import lnd.sample.misc.exception.DataConstraintException;
import lnd.sample.model.Group;
import lnd.sample.service.GroupService;
import lnd.sample.web.action.base.AbstractAction;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ModelDriven;

/** Change default name-space (default to package name) */
@Namespace(value = GroupAction.NAME_SPACE)
@Results({ @Result(name = AbstractAction.ACTION_LIST, location = AbstractAction.ACTION_LIST, type = AbstractAction.TYPE_REDIRECT_ACTION),
        @Result(name = AbstractAction.LIST, location = GroupAction.PAGE_LIST),
        @Result(name = com.opensymphony.xwork2.Action.ERROR, location = GroupAction.PAGE_CREATE),
        @Result(name = AbstractAction.GRID, location = "GroupsGrid.jsp"),
        @Result(name = com.opensymphony.xwork2.Action.INPUT, location = GroupAction.PAGE_CREATE) })
public class GroupAction extends AbstractAction implements ModelDriven<Group> {

    public static final String NAME_SPACE = "/group";
    private static final String MODEL_NAME = "Group"; /* Used in page names */
    private static final String MODEL_DISPLAY_NAME = "Group"; /* Used for localization */

    protected static final String PAGE_CREATE = "create" + MODEL_NAME + ".jsp";
    protected static final String PAGE_LIST = "list" + MODEL_NAME + "s.jsp";
    protected static final String PAGE_VIEW = "view" + MODEL_NAME + ".jsp";

    @Autowired
    private GroupService groupService;

    private String headerText;

    private Group group = new Group();

    private List<Group> results;

    private List<Long> ids;

    public List<Group> getResults() {
        return results;
    }

    public void setResults(List<Group> results) {
        this.results = results;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public List<Long> getIds() {
        return ids;
    }

    public void setIds(List<Long> ids) {
        this.ids = ids;
    }

    public String getHeaderText() {
        return headerText;
    }

    public void setHeaderText(String headerText) {
        this.headerText = headerText;
    }

    @SkipValidation
    @Action(value = ACTION_PRE_CREATE, results = { @Result(name = PREPARE, location = PAGE_CREATE) })
    public String create() throws DataConstraintException, Exception {
        try {
            initDataForCreate();
            return PREPARE;
        } catch (Exception e) {
            addActionError("Prepare data fail: " + e.getMessage());
            initDataForCreate();
            return ERROR;
        }
    }

    @Action(value = ACTION_CREATE)
    public String doCreate() throws DataConstraintException, Exception {
        try {
            this.groupService.save(group);
            return ACTION_LIST;
        } catch (Exception e) {
            addActionError("Create " + MODEL_DISPLAY_NAME + " fail");
            initDataForCreate();
            return ERROR;
        }
    }

    @SkipValidation
    @Action(value = ACTION_PRE_COPY, results = { @Result(name = PREPARE, location = PAGE_CREATE) })
    public String copy() throws DataConstraintException, Exception {
        try {
            initDataForCopy();
            loadDataModel(group);

            if (group == null) {
                group = new Group();
            } else {

                // Add suffix "-Copy" to some fields
                group.setCode(group.getCode() + "-Copy");
                group.setName(group.getName() + "-Copy");

                // Clear data
                group.setDescription(null);
            }

            return PREPARE;
        } catch (Exception e) {
            addActionError("Prepare data fail");
            initDataForCreate();
            return ERROR;
        }
    }

    @Action(value = ACTION_COPY, results = { @Result(name = ACTION_VIEW, location = ACTION_VIEW, type = AbstractAction.TYPE_REDIRECT_ACTION, params = {
            "id", "%{id}" }) })
    public String doCopy() throws DataConstraintException, Exception {
        try {
            this.groupService.save(group);

            pushModel(this.group);
            return ACTION_VIEW;
        } catch (Exception e) {
            addActionError("Copy " + MODEL_DISPLAY_NAME + " fail: " + e.getMessage());
            initDataForCreate();
            return ERROR;
        }
    }

    @SkipValidation
    @Action(value = ACTION_PRE_EDIT, results = { @Result(name = PREPARE, location = PAGE_CREATE) })
    public String edit() {
        try {
            initDataForEdit();
            loadDataModel(group);

            return PREPARE;
        } catch (Exception e) {
            addActionError("Prepare data fail: " + e.getMessage());
            initDataForEdit();
            return ERROR;
        }
    }

    @Action(value = ACTION_EDIT)
    public String doEdit() {
        try {
            this.groupService.update(group);
            return ACTION_LIST;
        } catch (Exception e) {
            addActionError("Edit " + MODEL_DISPLAY_NAME + " fail: " + e.getMessage());
            initDataForEdit();
            return ERROR;
        }
    }

    @SkipValidation
    @Action(value = ACTION_VIEW, results = { @Result(name = SUCCESS, location = PAGE_VIEW) })
    public String view() {
        try {
            pageTitle = "View " + MODEL_DISPLAY_NAME + " Detail";
            headerText = pageTitle;
            loadDataModel(group);
            return SUCCESS;
        } catch (Exception e) {
            // Add errors
            return SUCCESS;
        }
    }

    @SkipValidation
    @Action(value = ACTION_LIST)
    public String list() {
        setResults(groupService.findAll());
        initDataForList();
        return LIST;
    }

    private void initDataForCreate() {
        action = ACTION_CREATE;
        pageTitle = "Create New " + MODEL_DISPLAY_NAME;
        headerText = pageTitle;
    }

    private void initDataForCopy() {
        action = ACTION_COPY;
        pageTitle = "Copy " + MODEL_DISPLAY_NAME;
        headerText = pageTitle;
    }

    private void initDataForEdit() {
        action = ACTION_EDIT;
        pageTitle = "Edit " + MODEL_DISPLAY_NAME;
        headerText = pageTitle;
    }

    private void initDataForList() {
        pageTitle = "List " + MODEL_DISPLAY_NAME + "s";
        headerText = pageTitle;
    }

    private void loadDataModel(final Group model) {
        if (model != null && model.getId() != null) {
            this.group = groupService.find(model.getId());
            if (this.group != null) {
                pushModel(this.group);
            }
        }
    }

    @SkipValidation
    @Action(ACTION_DELETE)
    public String delete() {
        try {
            if (group != null && !isNullOrEmpty(group.getId())) {
                groupService.delete(group.getId());
            }

            setResults(groupService.findAll());
            return GRID;
        } catch (Exception e) {
            return ERROR;
        }
    }

    @SkipValidation
    @Action(value = ACTION_DELETE_MULTI)
    public String deleteMultiple() {
        if (!isNullOrEmpty(ids)) {
            groupService.delete(ids);
        }

        setResults(groupService.findAll());
        return GRID;
    }

    @Override
    public Group getModel() {
        return group;
    }
}

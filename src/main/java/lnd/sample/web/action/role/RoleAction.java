package lnd.sample.web.action.role;

import java.util.List;

import javax.validation.Valid;

import lnd.sample.misc.exception.DataConstraintException;
import lnd.sample.model.Role;
import lnd.sample.service.RoleService;
import lnd.sample.web.action.base.AbstractAction;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ModelDriven;

/** Change default name-space (default to package name) */
@Namespace(value = RoleAction.NAME_SPACE)
@Results({ @Result(name = AbstractAction.ACTION_LIST, location = AbstractAction.ACTION_LIST, type = AbstractAction.TYPE_REDIRECT_ACTION),
        @Result(name = AbstractAction.LIST, location = "listRoles.jsp"),
        @Result(name = com.opensymphony.xwork2.Action.ERROR, location = "createRole.jsp"),
        @Result(name = com.opensymphony.xwork2.Action.INPUT, location = "createRole.jsp"),
        @Result(name = AbstractAction.GRID, location = "RolesGrid.jsp") })
public class RoleAction extends AbstractAction implements ModelDriven<Role> {

    public static final String NAME_SPACE = "/role";

    private static final String MODEL_NAME = "Role";

    @Autowired
    private RoleService roleService;

    private String headerText;
    @Valid
    private Role role = new Role();

    private List<Role> roles;

    public List<Long> ids;

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getHeaderText() {
        return headerText;
    }

    public void setHeaderText(String headerText) {
        this.headerText = headerText;
    }

    @SkipValidation
    @Action(value = ACTION_PRE_CREATE, results = { @Result(name = PREPARE, location = "createRole.jsp") })
    public String create() throws DataConstraintException, Exception {
        try {
            initDataForCreate();
            return PREPARE;
        } catch (Exception e) {
            addActionError("Prepare data fail");
            initDataForCreate();
            return ERROR;
        }
    }

    @Action(value = ACTION_CREATE)
    public String doCreate() throws DataConstraintException, Exception {
        try {
            this.roleService.save(role);
            return ACTION_LIST;
        } catch (Exception e) {
            addActionError("Create " + MODEL_NAME + " fail");
            initDataForCreate();
            return ERROR;
        }
    }

    @SkipValidation
    @Action(value = ACTION_PRE_COPY, results = { @Result(name = PREPARE, location = "createRole.jsp") })
    public String copy() throws DataConstraintException, Exception {
        try {
            initDataForCopy();
            loadDataModel(role);

            if (role == null) {
                role = new Role();
            } else {

                // Add suffix "-Copy" to some fields
                role.setCode(role.getCode() + "-Copy");
                role.setName(role.getName() + "-Copy");

                // Clear data
                role.setDescription(null);
            }

            return PREPARE;
        } catch (Exception e) {
            addActionError("Prepare data fail");
            initDataForCreate();
            return ERROR;
        }
    }

    @Action(value = ACTION_COPY, results = { @Result(name = ACTION_VIEW, location = ACTION_VIEW, type = AbstractAction.TYPE_REDIRECT_ACTION, params = {
            "id", "%{id}" }) })
    public String doCopy() throws DataConstraintException, Exception {
        try {
            this.roleService.save(role);

            pushModel(this.role);
            return ACTION_VIEW;
        } catch (Exception e) {
            addActionError("Copy " + MODEL_NAME + " fail");
            initDataForCreate();
            return ERROR;
        }
    }

    @SkipValidation
    @Action(value = ACTION_PRE_EDIT, results = { @Result(name = PREPARE, location = "createRole.jsp") })
    public String edit() {
        try {
            initDataForEdit();
            loadDataModel(role);

            return PREPARE;
        } catch (Exception e) {
            addActionError("Prepare data fail");
            initDataForEdit();
            return ERROR;
        }
    }

    @Action(value = ACTION_EDIT)
    public String doEdit() {
        try {
            this.roleService.update(role);
            return ACTION_LIST;
        } catch (Exception e) {
            addActionError("Edit " + MODEL_NAME + " fail");
            initDataForEdit();
            return ERROR;
        }
    }

    @SkipValidation
    @Action(value = ACTION_VIEW, results = { @Result(name = SUCCESS, location = "viewRole.jsp") })
    public String view() {
        try {
            pageTitle = "View " + MODEL_NAME + " Detail";
            headerText = pageTitle;
            loadDataModel(role);
            return SUCCESS;
        } catch (Exception e) {
            // Add errors
            return SUCCESS;
        }
    }

    @SkipValidation
    @Action(value = ACTION_LIST)
    public String list() {
        this.roles = roleService.findAll();
        return LIST;
    }

    @SkipValidation
    @Action(value = ACTION_DELETE_MULTI)
    public String deleteMultiple() {
        if (!isNullOrEmpty(ids)) {
            roleService.delete(ids);
        }

        setRoles(roleService.findAll());
        return GRID;
    }

    private void initDataForCreate() {
        action = ACTION_CREATE;
        pageTitle = "Create New " + MODEL_NAME;
        headerText = pageTitle;
    }

    private void initDataForCopy() {
        action = ACTION_COPY;
        pageTitle = "Copy " + MODEL_NAME;
        headerText = pageTitle;
    }

    private void initDataForEdit() {
        action = ACTION_EDIT;
        pageTitle = "Edit " + MODEL_NAME;
        headerText = pageTitle;
    }

    private void loadDataModel(final Role model) {
        if (model != null && model.getId() != null) {
            this.role = roleService.find(model.getId());
            if (this.role != null) {
                pushModel(this.role);
            }
        }
    }

    @SkipValidation
    @Action(ACTION_DELETE)
    public String delete() {
        try {
            if (role != null && !isNullOrEmpty(role.getId())) {
                roleService.delete(role.getId());
            }

            setRoles(roleService.findAll());

            return GRID;
        } catch (Exception e) {
            return ERROR;
        }
    }

    @Override
    public Role getModel() {
        return role;
    }
}

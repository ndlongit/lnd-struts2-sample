package lnd.sample.misc.exception;

public class ValidationException extends AbstractException {

    public ValidationException(Throwable cause) {
        super(cause);
    }
}

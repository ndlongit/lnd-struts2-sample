package lnd.sample.misc.exception;

public class DataConstraintException extends AbstractException {

    public DataConstraintException(Throwable cause) {
        super(cause);
    }
}

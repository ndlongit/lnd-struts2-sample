package lnd.sample.misc.constant;

public class WebConstant {

    public static final String CSS_FOLDER = "context:layout";
    public static final String SCRIPTS_FOLDER = "context:scripts";
    public static final String IMAGES_FOLDER = "context:images";
    public static final String MAIN_FORM_NAME = "createEntityForm";

}

package lnd.sample.misc.search;

import lnd.sample.misc.search.core.AbstractSearch;
import lnd.sample.model.Group;
import lnd.sample.model.User;

public class UserSearch extends AbstractSearch {

    private User user;
    private Group group;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public UserSearch() {
        this(new User(), new Group());
    }

    public UserSearch(User user, Group group) {
        this.user = user;
        this.group = group;
    }
}

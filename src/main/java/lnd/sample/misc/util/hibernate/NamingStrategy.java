package lnd.sample.misc.util.hibernate;

import java.util.HashMap;
import java.util.Map;

import org.hibernate.cfg.ImprovedNamingStrategy;

public class NamingStrategy extends ImprovedNamingStrategy {

    public static final String TABLE_PREFIX = "";
    protected static final Map<String, String> TABLE_NAME_MAPPING;

    static {
        TABLE_NAME_MAPPING = new HashMap<String, String>();
        //@formatter:off
        String[][] temp = { 
                { "account", "accounts" }, 
		{ "group", "groups" }, 
		{ "right", "rights" },
		{ "role", "roles" }, 
		{ "user", "users" } 
	};
	//@formatter:on
        
        for (String[] array : temp) {
            TABLE_NAME_MAPPING.put(array[0], array[1]);
        }
    }

    protected static String getMappingName(String key) {
        String result = TABLE_NAME_MAPPING.get(key);

        if (result == null || "".equals(result)) {
            result = key;
        }

        return TABLE_PREFIX + result;
    }

    @Override
    public String classToTableName(String className) {
        return getMappingName(super.classToTableName(className));
    }

    @Override
    public String tableName(String tableName) {
        return getMappingName(super.tableName(tableName));
    }

    @Override
    public String propertyToColumnName(String propertyName) {
        return super.propertyToColumnName(propertyName);
    }

    @Override
    public String columnName(String columnName) {
        return super.columnName(columnName);
    }

    @Override
    public String collectionTableName(String ownerEntity, String ownerEntityTable, String associatedEntity, String associatedEntityTable, String propertyName) {

        /* Note: Duplication PREFIX if we add TABLE_PREFIX */
        return super.collectionTableName(ownerEntity, ownerEntityTable, associatedEntity, associatedEntityTable, propertyName);
    }

    @Override
    public String joinKeyColumnName(String joinedColumn, String joinedTable) {
        return super.joinKeyColumnName(joinedColumn, joinedTable);
    }

    @Override
    public String foreignKeyColumnName(String propertyName, String propertyEntityName, String propertyTableName, String referencedColumnName) {
        return super.foreignKeyColumnName(propertyName, propertyEntityName, propertyTableName, referencedColumnName);
    }

    @Override
    public String logicalCollectionColumnName(String columnName, String propertyName, String referencedColumn) {
        return TABLE_PREFIX + super.logicalCollectionColumnName(columnName, propertyName, referencedColumn);
    }
}

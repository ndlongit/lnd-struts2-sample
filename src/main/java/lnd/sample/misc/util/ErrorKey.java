package lnd.sample.misc.util;

public final class ErrorKey {

    public static final String COMMON_ERROR = "common.error";
    public static final String SAVE_ERROR = "common.error.save";
    public static final String UPDATE_ERROR = "common.error.update";

}

package lnd.sample.component;

public interface IStringEncryption {

    public String encode(CharSequence rawPassword);

    public boolean matches(CharSequence rawPassword, String encodedPassword);
}

package lnd.sample.model;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import lnd.sample.model.base.Account;

@Entity
@Table(name = "Users")
@PrimaryKeyJoinColumn(name = "user_id")
@XmlRootElement(name = "user")
@XmlAccessorType(XmlAccessType.FIELD)
public class User extends Account {

	public static void copy(User user) {
		user.setId(null);
		user.setLoginName(user.getLoginName() + "-Copy");
		user.setFirstName(user.getFirstName() + "-Copy");
		user.setLastName(user.getLastName() + "-Copy");
	}

	@Override
	public String toString() {
		return getFullName();
	}
}

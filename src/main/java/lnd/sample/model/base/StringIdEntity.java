package lnd.sample.model.base;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

@SuppressWarnings("serial")
@MappedSuperclass
public abstract class StringIdEntity implements BasicEntity<String> {

    private String id;

    public StringIdEntity() {
        setId(null);
    }

    public StringIdEntity(String id) {
        setId(id);
    }

    @Id
    // @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    @Transient
    public boolean isTransient() {
        return (getId() == null || "".equalsIgnoreCase(getId().trim()));
    }
}

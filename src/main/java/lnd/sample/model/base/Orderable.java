package lnd.sample.model.base;

public interface Orderable {

    public String getOrderByClause();
}

package lnd.sample.model.base;

public interface LogicalDeletion {

    public static final String PROP_DELETED = "deleted";
    public static final String NOT_DELETED = PROP_DELETED + " is null or " + PROP_DELETED + " = false";

    public void setDeleted(boolean deleted);

    public boolean isDeleted();
}

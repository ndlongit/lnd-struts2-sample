package lnd.sample.model.base;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Transient;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import lnd.sample.model.Group;
import lnd.sample.model.Role;

@Entity
// @Table(name = "Account")
@Inheritance(strategy = InheritanceType.JOINED)
public class Account extends NumericIdEntity {

    public static final String PROP_LOGIN_NAME = "loginName";

    public static final String PROP_FIRST_NAME = "firstName";
    public static final String PROP_LAST_NAME = "lastName";
    public static final String PROP_EMAIL = "email";

    public static final String PROP_GROUPS = "groups";
    public static final String PROP_ROLES = "roles";

    private String password;
    private String encryptedPassword;
    private String loginName;
    private String firstName;
    private String middleName;
    private String lastName;
    private Date dateOfBirth;
    private String email;
    private String phoneNumber;

    private String password2;

    public static enum Gender {
        MALE, FEMALE, NA
    }

    private Gender gender;

    private List<Group> groups;
    private List<Role> roles;

    @Column(name = "login_name", unique = true, nullable = false, length = 50)
    /** Bean validation */
    @NotEmpty(message = "This feild is required.")
    // @Size(min = 3, max = 50)
    public String getLoginName() {
        return this.loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    @Transient
    @Size(min = 5, message = "[${validatedValue}] is too short.")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEncryptedPassword() {
        return encryptedPassword;
    }

    public void setEncryptedPassword(String encryptedPassword) {
        this.encryptedPassword = encryptedPassword;
    }

    // @NotEmpty(message = "This feild is required.")
    @Size(min = 2, max = 30)
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    // @NotEmpty(message = "This feild is required.")
    @Size(min = 2, max = 30)
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    @Enumerated(EnumType.STRING)
    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    @Email
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @ManyToMany(targetEntity = Group.class, fetch = FetchType.LAZY)
    @JoinTable(name = "users_groups", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "group_id"))
    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    @ManyToMany(targetEntity = Role.class, fetch = FetchType.LAZY)
    @JoinTable(name = "users_roles", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
    @XmlElementWrapper(name = "roles")
    @XmlElement(name = "role")
    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    @Transient
    public String getPassword2() {
        return password2;
    }

    public void setPassword2(String password2) {
        this.password2 = password2;
    }

    @Transient
    public String getFullName() {
        String format = "%1s, %2s";
        
        if (!isNullOrEmpty(middleName)) {
            format += " %3s";
        }
        
        String fullName = String.format(format, firstName, lastName, middleName);
        return fullName.trim();
    }

    @Transient
    public String getUsername() {
        return getLoginName();
    }

    @Transient
    @Override
    public String toString() {
        return String.format("ID: %1s, LoginName: %2s", getId(), getLoginName());
    }

    @Transient
    public String getMailto() {
        String s = null;
        if (!isNullOrEmpty(this.email)) {
            s = String.format("<a href='mailto:%1$s?subject=%2$s' title='%1$s'>%1$s</a>", this.email, "Email%20Subject");
        }

        return s;
    }
}

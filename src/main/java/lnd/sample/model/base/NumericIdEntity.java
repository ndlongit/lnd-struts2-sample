package lnd.sample.model.base;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

import lnd.sample.misc.util.AppUtil;

@SuppressWarnings("serial")
@MappedSuperclass
public abstract class NumericIdEntity extends AbstractLogicalDeletion implements BasicEntity<Long>, LogicalDeletion {

    private Long id;

    @Id
    @Override
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    @Transient
    public boolean isTransient() {
        return (getId() == null || getId() <= 0);
    }

    @Transient
    public static boolean isNullOrEmpty(Object value) {
        return AppUtil.isNullOrEmpty(value);
    }

    @Transient
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof NumericIdEntity)) {
            return false;
        }

        NumericIdEntity other = (NumericIdEntity) obj;
        if (getId() != null && getId().equals(other.getId())) {
            return true;
        }

        return false;
    }
    
    @Override
    public int hashCode() {
        return super.hashCode();
    }
}

package lnd.sample.model.base;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
// @SQLDelete(sql = "UPDATE Role SET " + LogicalDeletion.PROP_DELETED + " = true WHERE " + BasicEntity.PROP_ID + "id = ?")
// @Where(clause = "WHERE " + LogicalDeletion.NOT_DELETED)
public abstract class AbstractLogicalDeletion implements LogicalDeletion {

    private boolean deleted;

    @Override
    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public boolean isDeleted() {
        return deleted;
    }
}

package lnd.sample.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;

import lnd.sample.misc.listener.MyEntityListener;
import lnd.sample.model.base.Account;

import org.hibernate.annotations.ForeignKey;

@Entity
@EntityListeners(MyEntityListener.class)
@PrimaryKeyJoinColumn(name = "employee_id")
public class Employee extends Account {

    private String code;
    private Double salary;
    private Employee manager;
    private JobTitle jobTitle;
    private Department department;

    @Column(name = "employee_code", nullable = false, unique = true, length = 10)
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @ManyToOne
    @ForeignKey(name = "manager_id_fk")
    public Employee getManager() {
        return manager;
    }

    public void setManager(Employee manager) {
        this.manager = manager;
    }

    @ManyToOne
    @JoinColumn
    public JobTitle getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(JobTitle jobTitle) {
        this.jobTitle = jobTitle;
    }

    @ManyToOne
    @JoinColumn(name = "department_id")
    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }
}

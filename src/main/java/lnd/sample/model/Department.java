package lnd.sample.model;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;

import lnd.sample.model.base.SimpleEntity;

@Entity
@AttributeOverride(name = SimpleEntity.PROP_CODE, column = @Column(name = "code", unique = true, nullable = false))
public class Department extends SimpleEntity {
}
